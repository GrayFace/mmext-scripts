event 1
  4:  Exit  {}

  5:  Subtract  {"HasFullHP", Value = 131073}
  2:  Cmd00  {}
end

event 2
  4:  EnterHouse  {Id = 33554948}         -- not found!
  2:  Cmd00  {}
end

event 3
  4:  PlaySound  {Id = 33555204, X = 263168, Y = 67372036}
  2:  Cmd00  {}
end

event 4
  4:  Hint  {Str = 4}
  2:  Cmd00  {}
end

event 5
  4:  MazeInfo  {Str = 4}
  2:  Cmd00  {}
end

event 6
  4:  MoveToMap  {X = 33555972, Y = 459776, Z = 117704452, Direction = 67109376, LookAngle = 134479880, SpeedZ = 33556484, HouseId = 0, Icon = 4, Name = "\9"}
  2:  Cmd00  {}
end

event 7
  4:  OpenChest  {Id = 4}
  2:  Cmd00  {}
end

event 8
  4:  FaceExpression  {Player = "Current", Frame = 8}
  2:  Cmd00  {}
end

event 9
  4:  DamagePlayer  {Player = "Current", -- ERROR: Const not found
DamageType = 9, Damage = 67109376}
  2:  Cmd00  {}
end

event 10
  4:  SetSnow  {EffectId = 4, On = true}
  2:  Cmd00  {}
end

event 266
  6:  Cmd00  {}
end

event 11
  4:  SetTexture  {Facet = 33557252, Name = ""}
  2:  Cmd00  {}
end

event 12
  4:  SetTextureOutdoors  {Model = 33557508, Facet = 852992, Name = "\4\13\4\13"}
  2:  Cmd00  {}
end

event 13
  4:  SetSprite  {SpriteId = 33557764, Visible = 0, Name = "\4\14"}
  2:  Cmd00  {}
end

event 14
  4:  Cmp  {"HasFullHP", Value = 131086,   jump = 4}
  2:  Cmd00  {}
end

event 15
  4:  SetDoorState  {Id = 4, State = 15}
  2:  Cmd00  {}
end

event 16
  4:  Add  {"HasFullHP", Value = 131088}
  2:  Cmd00  {}
end

event 17
  4:  Subtract  {"HasFullHP", Value = 131089}
  2:  Cmd00  {}
end

event 18
  4:  Set  {"HasFullHP", Value = 131090}
  2:  Cmd00  {}
end

event 19
  4:  SummonMonsters  {TypeIndexInMapStats = 4, Level = 19, Count = 0, X = 335806466, Y = 68420608, Z = 131092}
  2:  Cmd00  {}
end

event 20
  4:  Cmd14  {unk_1 = 33559556, unk_2 = 0}
  2:  Cmd00  {}
end

event 21
  4:  CastSpell  {Spell = 4, Mastery = 22, Skill = 0, FromX = 369360898, FromY = 68551680, FromZ = 131094, ToX = 67114756, ToY = 1508375, ToZ = 402915330}         -- "Fire Bolt"
  2:  Cmd00  {}
end

event 22
  4:  SpeakNPC  {NPC = 33560068}         -- not found!
  2:  Cmd00  {}
end

event 23
  4:  SetFacetBit  {Id = 33560324, -- ERROR: Const not found
Bit = const.FacetBits.IsEventJustHint + 0x80000 + const.FacetBits.ProjectToYZ, On = true}
  2:  Cmd00  {}
end

event 24
  4:  SetFacetBitOutdoors  {Model = 33560580, Facet = 1639424, -- ERROR: Const not found
-- ERROR: Const not found
-- ERROR: Const not found
Bit = const.FacetBits.TriggerByObject + const.FacetBits.TriggerByMonster + const.FacetBits.FlipV + const.FacetBits.MoveByDoor + 0x1000 + 0x800 + const.FacetBits.ProjectToXY + 0x4, On = false}
  2:  Cmd00  {}
end

event 25
  4:  RandomGoTo  {jumpA = 4, jumpB = 25, jumpC = 0, jumpD = 2, jumpE = 0, jumpF = 4}
  2:  Cmd00  {}
end

event 26
  4:  Question  {Question = 33561092, Answer1 = 1770496, Answer2 = 453253892,   jump(ok) = 0}
  2:  Cmd00  {}
end

event 27
  4:  Cmd1B  {unk_1 = 4, unk_2 = 27}
  2:  Cmd00  {}
end

event 28
  4:  Cmd1C  {unk_1 = 4}
  2:  Cmd00  {}
end

event 29
  4:  StatusText  {Str = 33561860}
  2:  Cmd00  {}
end

event 30
  4:  SetMessage  {Str = 33562116}
  2:  Cmd00  {}
end

event 31
  4:  OnTimer  {EachYear = true, EachMonth = true, Hour = 2, Second = 4, IntervalInHalfMinutes = 32}
  2:  Cmd00  {}
end

event 32
  4:  SetLight  {Id = 33562628, On = false}
  2:  Cmd00  {}
end

event 33
  4:  SimpleMessage  {}
  2:  Cmd00  {}
end

event 35
  4:  ForPlayer  ("Current")
  2:  Cmd00  {}
end

event 36
  4:  GoTo  {jump = 4}

  2:  Cmd00  {}
end

event 37
  4:  OnLoadMap  {}
  2:  Cmd00  {}
end

event 38
  4:  OnRefillTimer  {EachYear = true, EachMonth = true, Hour = 2, Second = 4, IntervalInHalfMinutes = 39}
  2:  Cmd00  {}
end

event 39
  4:  SetNPCTopic  {NPC = 33564420, Index = 0, Event = 67119108}         -- not found! : not found!
  2:  Cmd00  {}
end

event 40
  4:  MoveNPC  {NPC = 33564676, HouseId = 2688000}         -- not found! -> not found!
  2:  Cmd00  {}
end

event 41
  4:  GiveItem  {Strength = 4, Type = const.ItemType.Amulet_, Id = 67109376}         -- not found!
  2:  Cmd00  {}
end

event 42
  4:  ChangeEvent  {NewEvent = 33565188}         -- not found!
  2:  Cmd00  {}
end

event 43
  4:  CheckSkill  {const.Skills.Spear, Mastery = 44, Level = 67109376,   jump(>=) = 44}
  2:  Cmd00  {}
end

event 44

  2:  Cmd00  {}
end

event 45

  2:  Cmd00  {}
end

event 46

  2:  Cmd00  {}
end

event 47

  2:  Cmd00  {}
end

event 48

  2:  Cmd00  {}
end

event 49

  2:  Cmd00  {}
end

event 50

  2:  Cmd00  {}
end

event 51

  2:  Cmd00  {}
end

event 52

  2:  Cmd00  {}
end

event 53

  2:  Cmd00  {}
end

event 54

  2:  Cmd00  {}
end

event 55

  2:  Cmd00  {}
end

event 56

  2:  Cmd00  {}
end

event 57

  2:  Cmd00  {}
end

event 58

  2:  Cmd00  {}
end

event 59

  2:  Cmd00  {}
end

event 60

  2:  Cmd00  {}
end

event 61

  2:  Cmd00  {}
end

event 62

  2:  Cmd00  {}
end

event 63

  2:  Cmd00  {}
end

event 64

  2:  Cmd00  {}
end

event 65

  2:  Cmd00  {}
end

event 66

  2:  Cmd00  {}
end

event 67

  2:  Cmd00  {}
end

event 68

  2:  Cmd00  {}
end

event 69

  2:  Cmd00  {}
end

event 70

  2:  Cmd00  {}
end

event 71

  2:  Cmd00  {}
end

event 72

  2:  Cmd00  {}
end

event 73

  2:  Cmd00  {}
end

event 74

  2:  Cmd00  {}
end

event 75

  2:  Cmd00  {}
end

event 76

  2:  Cmd00  {}
end

event 77

  2:  Cmd00  {}
end

event 78

  2:  Cmd00  {}
end

event 79

  2:  Cmd00  {}
end

event 80

  2:  Cmd00  {}
end

event 81

  1:  Cmd00  {}
end

event 82

  1:  Cmd00  {}
end

event 83

  1:  Cmd00  {}
end

event 84

  1:  Cmd00  {}
end

event 85

  1:  Cmd00  {}
end

event 86

  1:  Cmd00  {}
end

event 87

  1:  Cmd00  {}
end

event 89

end

event 90

  2:  Cmd00  {}
end
