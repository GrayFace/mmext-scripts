str[0] = " "
str[1] = "Sack"
str[2] = "You find jerked beef in the sack (+10 food)."
str[3] = "Mead filled barrel."
str[4] = "You drink the mead and are feeling good."
str[5] = "Stew filled bowl."
str[6] = "The stew tasts good - you feel better (+5 hits)."
str[7] = "Crates filled with wood chips."
str[8] = "The sack is empty."
str[9] = "The bowl is empty."
str[10] = "There is of nothing of value in the crates."
str[11] = "The mead barrel is empty."
str[12] = ""


event 1
      Hint = str[1]  -- "Sack"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 5}
  1:  StatusText  {Str = 2}         -- "You find jerked beef in the sack (+10 food)."
  2:  Add  {"Food", Value = 10}
  3:  Add  {"MapVar0", Value = 1}
  4:  Exit  {}

  5:  StatusText  {Str = 8}         -- "The sack is empty."
end

event 2
      Hint = str[5]  -- "Stew filled bowl."
  0:  Cmp  {"MapVar1", Value = 2,   jump = 5}
  1:  StatusText  {Str = 6}         -- "The stew tasts good - you feel better (+5 hits)."
  2:  Add  {"HP", Value = 5}
  3:  Add  {"MapVar1", Value = 1}
  4:  Exit  {}

  5:  StatusText  {Str = 9}         -- "The bowl is empty."
end

event 3
      Hint = str[7]  -- "Crates filled with wood chips."
  0:  StatusText  {Str = 10}         -- "There is of nothing of value in the crates."
end

event 4
      Hint = str[3]  -- "Mead filled barrel."
  0:  Cmp  {"MapVar2", Value = 4,   jump = 5}
  1:  StatusText  {Str = 4}         -- "You drink the mead and are feeling good."
  2:  Add  {"Drunk", Value = 5}
  3:  Add  {"MapVar2", Value = 1}
  4:  Exit  {}

  5:  StatusText  {Str = 11}         -- "The mead barrel is empty."
end
