str[0] = " "
str[1] = "Switch."
str[2] = "With some muscle you move the switch down."
str[3] = "The switch is stuck, you need someone stronger to move it."
str[4] = "Crates."
str[5] = "The crates are all empty."
str[6] = "Door."
str[7] = "You need a special key to open this door."
str[8] = ""
str[9] = "Light red liquid filled keg."
str[10] = "Your muscles expand as you take a drink."
str[11] = "The liquid has lost its effectiveness."
str[12] = "You flip the switch and hear grating off in the distance."
str[13] = "A skeletal knight steps out of a forming mist and says -To let you pass I will need you to say a prayer over my bones found close by, when this is done you can pass-"
str[14] = "Old bones."
str[15] = "The bones feel incredibly cold to the touch."
str[16] = "You say a prayer over the bones. "
str[17] = "The skeletal knight appears from the mist, nods and disappears into the mist again."
str[18] = "Something slimey moves behind this brick wall."
str[19] = "Wall with missing bricks."
str[20] = "You find a grey key where one of the bricks used to be."
str[21] = "You find a silver key where one of the bricks used to be."
str[22] = "Chest."
str[23] = "You need a key to open this chest."
str[24] = "A skeletal looking cleric steps out of a quick forming mist and says - I was tortured down here long ago and my bones were scattered throughout this level, I will let you pass only when you have destroyed what remains of me and return here, you will feel which bones they are when you see them...-  He fades away."
str[25] = "You find and destroy one of the clerics bones. "
str[26] = "The cleric appears again and nods at you -Thank you I will never forget your kindness I can rest in peace at last -  He then fades away."
str[27] = "You get a bad feeling from the bones."
str[28] = "Iron Maiden"
str[29] = "There's nothing but spikes in the Iron maiden."
str[30] = "You search the Burial niche and find some gold."
str[31] = "The bones seems to charge with electricity !!."
str[32] = "These are really old bones...really old."
str[33] = "You gain knowledge from these ancient bones."
str[34] = "Long dead adventurer."
str[35] = "You search the body and find some gold."
str[36] = "Button"
str[37] = "A mist forms and a shadowy figure of a skeletal noblemen steps in front of you saying -The undead are restless in this tomb, before I can let you leave you must visit each one of them- the noblemen shoves you back and then vanishes."
str[38] = "The skeletal noblemen appears from the mist, nods his head, then vanishes."
str[39] = "Burial niche"
str[40] = "Skeleton in a cage."
str[41] = "The skeleton grabs for you."
str[42] = "A strange force reaches out of the wall and grabs you."
str[43] = "The door will not budge."
str[44] = "Sack"
str[45] = "You find some half decent food in the sack."
str[46] = "A scrawled message on the brick reads   2=1  3=2  1=3"
str[47] = ""


event 1
      Hint = str[1]  -- "Switch."
  0:  StatusText  {Str = 2}         -- "With some muscle you move the switch down."
  1:  SetDoorState  {Id = 1, State = 2}         -- switch state
  2:  SetDoorState  {Id = 3, State = 2}         -- switch state
end

event 2
      Hint = str[1]  -- "Switch."
  0:  StatusText  {Str = 2}         -- "With some muscle you move the switch down."
  1:  SetDoorState  {Id = 2, State = 2}         -- switch state
  2:  SetDoorState  {Id = 4, State = 2}         -- switch state
end

event 3
      Hint = str[4]  -- "Crates."
  0:  StatusText  {Str = 5}         -- "The crates are all empty."
end

event 4
      Hint = str[6]  -- "Door."
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Switch."
  0:  Cmp  {"MapVar5", Value = 1,   jump = 5}
  1:  StatusText  {Str = 12}         -- "You flip the switch and hear grating off in the distance."
  2:  SetTexture  {Facet = 1392, Name = "d8s2on"}
  3:  SetDoorState  {Id = 6, State = 2}         -- switch state
  4:  Add  {"MapVar5", Value = 1}
  5:  Exit  {}
end

event 8
      Hint = str[14]  -- "Old bones."
  0:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Elec, Damage = 10}
end

event 9
      Hint = str[19]  -- "Wall with missing bricks."
  0:  SetMessage  {Str = 18}         -- "Something slimey moves behind this brick wall."
  1:  SimpleMessage  {}
  2:  Add  {"Afraid", Value = 1}
end

event 10
      Hint = str[19]  -- "Wall with missing bricks."
  0:  StatusText  {Str = 18}         -- "Something slimey moves behind this brick wall."
  1:  Add  {"Afraid", Value = 2}
end

event 11
      Hint = str[19]  -- "Wall with missing bricks."
  0:  StatusText  {Str = 18}         -- "Something slimey moves behind this brick wall."
  1:  Add  {"Afraid", Value = 2}
end

event 12
      Hint = str[22]  -- "Chest."
  0:  OpenChest  {Id = 0}
end

event 13
      Hint = str[22]  -- "Chest."
  0:  OpenChest  {Id = 1}
end

event 14
  0:  SetDoorState  {Id = 7, State = 1}
  1:  SetDoorState  {Id = 8, State = 1}
end

event 19
      Hint = str[14]  -- "Old bones."
  0:  StatusText  {Str = 27}         -- "You get a bad feeling from the bones."
  1:  ForPlayer  ("Random")
  2:  Add  {"Cursed", Value = 1}
end

event 20
      Hint = str[28]  -- "Iron Maiden"
  0:  StatusText  {Str = 29}         -- "There's nothing but spikes in the Iron maiden."
end

event 21
      Hint = str[36]  -- "Button"
  0:  Cmp  {"MapVar19", Value = 2,   jump = 8}
  1:  SetDoorState  {Id = 10, State = 0}
  2:  SetDoorState  {Id = 11, State = 0}
  3:  SetDoorState  {Id = 9, State = 1}
  4:  Set  {"MapVar19", Value = 0}
  5:  Set  {"MapVar18", Value = 0}
  6:  Add  {"MapVar16", Value = 1}
  7:  Exit  {}

  8:  SetDoorState  {Id = 9, State = 2}         -- switch state
  9:  SetDoorState  {Id = 12, State = 2}         -- switch state
end

event 22
      Hint = str[36]  -- "Button"
  0:  Cmp  {"MapVar16", Value = 1,   jump = 5}
  1:  Cmp  {"MapVar18", Value = 1,   jump = 5}
  2:  Add  {"MapVar19", Value = 1}
  3:  SetDoorState  {Id = 10, State = 1}
  4:  Exit  {}

  5:  SetDoorState  {Id = 9, State = 0}
  6:  SetDoorState  {Id = 10, State = 1}
  7:  SetDoorState  {Id = 11, State = 0}
  8:  Set  {"MapVar16", Value = 0}
  9:  Set  {"MapVar18", Value = 0}
end

event 23
      Hint = str[36]  -- "Button"
  0:  Cmp  {"MapVar19", Value = 1,   jump = 8}
  1:  SetDoorState  {Id = 9, State = 0}
  2:  SetDoorState  {Id = 10, State = 0}
  3:  SetDoorState  {Id = 11, State = 1}
  4:  Add  {"MapVar18", Value = 1}
  5:  Set  {"MapVar19", Value = 0}
  6:  Set  {"MapVar16", Value = 0}
  7:  Exit  {}

  8:  Add  {"MapVar19", Value = 1}
  9:  SetDoorState  {Id = 11, State = 1}
end

event 25
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar22", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 300}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 400}
  13: Add  {"MapVar22", Value = 1}
  14: Exit  {}
end

event 26
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar23", Value = 1,   jump = 15}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 12, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 300}
  4:  GoTo  {jump = 14}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 14}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  SimpleMessage  {}
  10: Add  {"AgeBonus", Value = 5}
  11: GoTo  {jump = 14}

  12: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  13: Add  {"Experience", Value = 400}
  14: Add  {"MapVar23", Value = 1}
  15: Exit  {}
end

event 27
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar24", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 300}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 400}
  13: Add  {"MapVar24", Value = 1}
  14: Exit  {}
end

event 28
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar25", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 300}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 800}
  13: Add  {"MapVar25", Value = 1}
  14: Exit  {}
end

event 29
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar26", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 300}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 400}
  13: Add  {"MapVar26", Value = 1}
  14: Exit  {}
end

event 30
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar27", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 300}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 400}
  13: Add  {"MapVar27", Value = 1}
  14: Exit  {}
end

event 31
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar28", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 300}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 400}
  13: Add  {"MapVar28", Value = 1}
  14: Exit  {}
end

event 32
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar29", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 500}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 900}
  13: Add  {"MapVar29", Value = 1}
  14: Exit  {}
end

event 33
      Hint = str[39]  -- "Burial niche"
  0:  Cmp  {"MapVar30", Value = 1,   jump = 14}
  1:  RandomGoTo  {jumpA = 2, jumpB = 5, jumpC = 8, jumpD = 11, jumpE = 0, jumpF = 0}
  2:  StatusText  {Str = 30}         -- "You search the Burial niche and find some gold."
  3:  Add  {"GoldAddRandom", Value = 400}
  4:  GoTo  {jump = 13}

  5:  StatusText  {Str = 31}         -- "The bones seems to charge with electricity !!."
  6:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Fire, Damage = 10}
  7:  GoTo  {jump = 13}

  8:  StatusText  {Str = 32}         -- "These are really old bones...really old."
  9:  Add  {"AgeBonus", Value = 5}
  10: GoTo  {jump = 13}

  11: StatusText  {Str = 33}         -- "You gain knowledge from these ancient bones."
  12: Add  {"Experience", Value = 400}
  13: Add  {"MapVar30", Value = 1}
  14: Exit  {}
end

event 34
      Hint = str[34]  -- "Long dead adventurer."
  0:  Cmp  {"MapVar31", Value = 1,   jump = 4}
  1:  StatusText  {Str = 35}         -- "You search the body and find some gold."
  2:  Add  {"GoldAddRandom", Value = 300}
  3:  Add  {"MapVar31", Value = 1}
  4:  Exit  {}
end

event 35
      Hint = str[40]  -- "Skeleton in a cage."
  0:  StatusText  {Str = 41}         -- "The skeleton grabs for you."
  1:  Add  {"Afraid", Value = 1}
end

event 37
  0:  StatusText  {Str = 42}         -- "A strange force reaches out of the wall and grabs you."
  1:  MoveToMap  {X = 12416, Y = 3200, Z = -2304, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 38
      Hint = str[6]  -- "Door."
  0:  StatusText  {Str = 43}         -- "The door will not budge."
  1:  SimpleMessage  {}
end

event 39
      Hint = str[44]  -- "Sack"
  0:  Cmp  {"MapVar39", Value = 1,   jump = 4}
  1:  StatusText  {Str = 45}         -- "You find some half decent food in the sack."
  2:  Add  {"Food", Value = 5}
  3:  Add  {"MapVar39", Value = 1}
  4:  Exit  {}
end

event 40
      Hint = str[44]  -- "Sack"
  0:  Cmp  {"MapVar40", Value = 1,   jump = 4}
  1:  StatusText  {Str = 45}         -- "You find some half decent food in the sack."
  2:  Add  {"Food", Value = 5}
  3:  Add  {"MapVar40", Value = 1}
  4:  Exit  {}
end

event 41
      Hint = str[44]  -- "Sack"
  0:  Cmp  {"MapVar41", Value = 1,   jump = 4}
  1:  StatusText  {Str = 45}         -- "You find some half decent food in the sack."
  2:  Add  {"Food", Value = 5}
  3:  Add  {"MapVar41", Value = 1}
  4:  Exit  {}
end

event 42
      Hint = str[6]  -- "Door."
  0:  StatusText  {Str = 43}         -- "The door will not budge."
end

event 43
      Hint = str[6]  -- "Door."
  0:  SetMessage  {Str = 46}         -- "A scrawled message on the brick reads   2=1  3=2  1=3"
  1:  SimpleMessage  {}
end
