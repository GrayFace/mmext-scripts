str[0] = " "
str[1] = "Tree"
str[2] = "There doesn't seem to be anymore apples."
str[3] = "Chest"
str[4] = "Drink from Well."
str[5] = "Drink from Fountain."
str[6] = "Drink from Trough."
str[7] = "Refreshing!"
str[8] = "+10 Spell points restored."
str[9] = "+10 Intellect and Personality temporary."
str[10] = "+5 Elemental resistance temporary."
str[11] = "+20 Luck temporary."
str[12] = "Poison!"
str[13] = "Welcome to Ellesia"
str[14] = "Ellesia"
str[15] = "Shrine of Might"
str[16] = "You pray at the shrine."
str[17] = "+10 Might permanent"
str[18] = "+3 Might permanent"
str[19] = "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                           iNs_u_t_rfesh_'ns"
str[20] = "Obelisk"
str[21] = "The Barrel is empty."
str[22] = "Green Super Barrel"
str[23] = "Thank you!"
str[24] = "A caged Prisoner!"
str[25] = "Closed for repairs."
str[26] = "You need a key to enter the temple."
str[27] = "Campfire"
str[28] = "Elixir of Revelation"
str[29] = ""


event 1
      Hint = str[1]  -- "Tree"
      MazeInfo = str[14]  -- "Ellesia"
  0:  Exit  {}
end

event 2
      Hint = str[5]  -- "Drink from Fountain."
      MazeInfo = str[14]  -- "Ellesia"
  0:  EnterHouse  {Id = 5}         -- "Arm's Length Spear Shop"
end

event 3
      Hint = str[5]  -- "Drink from Fountain."
  0:  Exit  {}

  1:  EnterHouse  {Id = 5}         -- "Arm's Length Spear Shop"
end

event 4
      Hint = str[16]  -- "You pray at the shrine."
  0:  EnterHouse  {Id = 16}         -- "Armor Emporium"
end

event 5
      Hint = str[16]  -- "You pray at the shrine."
  0:  Exit  {}

  1:  EnterHouse  {Id = 16}         -- "Armor Emporium"
end

event 6
      Hint = str[30]
  0:  EnterHouse  {Id = 30}         -- "Witch's Brew"
end

event 7
      Hint = str[30]
  0:  Exit  {}
end

event 8
      Hint = str[43]
  0:  EnterHouse  {Id = 43}         -- "Lock, Stock, and Barrel"
end

event 9
      Hint = str[43]
  0:  Exit  {}

  1:  EnterHouse  {Id = 43}         -- "Lock, Stock, and Barrel"
end

event 10
      Hint = str[60]
  0:  EnterHouse  {Id = 60}         -- "Wyvern's Wind"
end

event 11
      Hint = str[86]
  0:  EnterHouse  {Id = 86}         -- "Island Testing Center"
end

event 12
      Hint = str[86]
  0:  Exit  {}

  1:  EnterHouse  {Id = 86}         -- "Island Testing Center"
end

event 13
      Hint = str[90]
  0:  StatusText  {Str = 25}         -- "Closed for repairs."
  1:  Exit  {}

  2:  EnterHouse  {Id = 90}         -- "Town Hall"
end

event 14
      Hint = str[74]
  0:  ForPlayer  ("All")
  1:  Cmp  {"NPCs", Value = 110,   jump = 4}         -- "Tobin"
  2:  EnterHouse  {Id = 74}         -- "Ellesian Ministries"
  3:  Exit  {}

  4:  Subtract  {"NPCs", Value = 110}         -- "Tobin"
  5:  MoveNPC  {NPC = 175, HouseId = 74}         -- "Tobin" -> "Ellesian Ministries"
  6:  Subtract  {"QBits", Value = 83}         -- "Escourt Tobin back to the Misty Island Temple."
  7:  Set  {"Awards", Value = 63}         -- "Returned Tobin to the Ellesian Ministries."
  8:  Add  {"Experience", Value = 3000}
  9:  Add  {"Gold", Value = 1000}
  10: Add  {"ReputationIs", Value = 25}
  11: StatusText  {Str = 23}         -- "Thank you!"
  12: SetNPCTopic  {NPC = 154, Index = 0, Event = 328}         -- "Graham" : "Have you seen Tamara?"
  13: GoTo  {jump = 2}
end

event 15
      Hint = str[93]
  0:  EnterHouse  {Id = 93}         -- "The Orc Slayer"
end

event 16
      Hint = str[93]
  0:  Exit  {}

  1:  EnterHouse  {Id = 93}         -- "The Orc Slayer"
end

event 17
      Hint = str[114]
  0:  EnterHouse  {Id = 114}         -- "The Reserves"
end

event 18
      Hint = str[114]
  0:  Exit  {}

  1:  EnterHouse  {Id = 114}         -- "The Reserves"
end

event 19
      Hint = str[119]
  0:  EnterHouse  {Id = 119}         -- "Initiate Guild of Ignis"
end

event 20
      Hint = str[119]
  0:  Exit  {}

  1:  EnterHouse  {Id = 119}         -- "Initiate Guild of Ignis"
end

event 21
      Hint = str[121]
  0:  EnterHouse  {Id = 121}         -- "Initiate Guild of Aeros"
end

event 22
      Hint = str[121]
  0:  Exit  {}

  1:  EnterHouse  {Id = 121}         -- "Initiate Guild of Aeros"
end

event 23
      Hint = str[123]
  0:  EnterHouse  {Id = 123}         -- "Initiate Guild of Aqua Magics"
end

event 24
      Hint = str[123]
  0:  Exit  {}

  1:  EnterHouse  {Id = 123}         -- "Initiate Guild of Aqua Magics"
end

event 25
      Hint = str[142]
  0:  EnterHouse  {Id = 142}         -- "Duelists' Edge"
end

event 26
      Hint = str[142]
  0:  Exit  {}

  1:  EnterHouse  {Id = 142}         -- "Duelists' Edge"
end

event 27
      Hint = str[148]
  0:  EnterHouse  {Id = 148}         -- "Buccaneers' Lair"
end

event 28
      Hint = str[148]
  0:  Exit  {}

  1:  EnterHouse  {Id = 148}         -- "Buccaneers' Lair"
end

event 29
      Hint = str[90]
  0:  Exit  {}

  1:  EnterHouse  {Id = 90}         -- "Town Hall"
end

event 30
      Hint = str[110]
  0:  ForPlayer  ("Current")
  1:  Cmp  {"DiplomacySkill", Value = 136,   jump = 6}
  2:  SetNPCTopic  {NPC = 183, Index = 0, Event = 354}         -- "Guard" : "Audience with the Beneficent One"
  3:  SpeakNPC  {NPC = 183}         -- "Guard"
  4:  Exit  {}

  5:  MoveToMap  {X = 0, Y = 0, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 155, Icon = 1, Name = "0"}         -- "Enchanted Bastion"
  6:  EnterHouse  {Id = 156}         -- "Enchanted Bastion"
end

event 31
      Hint = str[13]  -- "Welcome to Ellesia"
  0:  Exit  {}
end

event 32
      Hint = str[22]  -- "Green Super Barrel"
  0:  ForPlayer  ("All")
  1:  Cmp  {"QBits", Value = 506,   jump = 6}         -- Green Super Barrel
  2:  Add  {"BaseEndurance", Value = 10}
  3:  Set  {"BodybuildingSkill", Value = 68}
  4:  Set  {"QBits", Value = 506}         -- Green Super Barrel
  5:  Exit  {}

  6:  StatusText  {Str = 21}         -- "The Barrel is empty."
  7:  Exit  {}
end

event 33
      Hint = str[24]  -- "A caged Prisoner!"
  0:  Cmp  {"QBits", Value = 500,   jump = 4}         -- Tobin Cage
  1:  StatusText  {Str = 23}         -- "Thank you!"
  2:  Set  {"NPCs", Value = 110}         -- "Tobin"
  3:  Set  {"QBits", Value = 500}         -- Tobin Cage
  4:  Exit  {}
end

event 34
      Hint = str[27]  -- "Campfire"
  0:  Cmp  {"QBits", Value = 501,   jump = 4}         -- Campfire
  1:  Add  {"Food", Value = 2}
  2:  Set  {"QBits", Value = 501}         -- Campfire
  3:  GiveItem  {Strength = 5, Type = const.ItemType.Misc, Id = 0}
  4:  Exit  {}
end

event 35
      Hint = str[27]  -- "Campfire"
  0:  Cmp  {"QBits", Value = 502,   jump = 4}         -- Campfire
  1:  Add  {"Food", Value = 1}
  2:  Set  {"QBits", Value = 502}         -- Campfire
  3:  GiveItem  {Strength = 5, Type = const.ItemType.Sword, Id = 0}
  4:  Exit  {}
end

event 36
      Hint = str[27]  -- "Campfire"
  0:  Cmp  {"QBits", Value = 503,   jump = 4}         -- Campfire
  1:  Add  {"Food", Value = 1}
  2:  Set  {"QBits", Value = 503}         -- Campfire
  3:  GiveItem  {Strength = 5, Type = const.ItemType.Armor_, Id = 0}
  4:  Exit  {}
end

event 50
      Hint = str[199]
  0:  EnterHouse  {Id = 455}         -- "House"
end

event 51
      Hint = str[200]
  0:  EnterHouse  {Id = 456}         -- "House"
end

event 52
      Hint = str[201]
  0:  EnterHouse  {Id = 457}         -- "House"
end

event 53
      Hint = str[202]
  0:  EnterHouse  {Id = 458}         -- "House"
end

event 54
      Hint = str[203]
  0:  EnterHouse  {Id = 459}         -- "House"
end

event 55
      Hint = str[204]
  0:  EnterHouse  {Id = 460}         -- "House"
end

event 56
      Hint = str[205]
  0:  EnterHouse  {Id = 461}         -- "House"
end

event 57
      Hint = str[206]
  0:  EnterHouse  {Id = 462}         -- "House"
end

event 58
      Hint = str[207]
  0:  EnterHouse  {Id = 463}         -- "House"
end

event 59
      Hint = str[208]
  0:  EnterHouse  {Id = 464}         -- "House"
end

event 60
      Hint = str[26]  -- "You need a key to enter the temple."
  0:  EnterHouse  {Id = 538}         -- "House of  Magnus"
end

event 61
      Hint = str[27]  -- "Campfire"
  0:  EnterHouse  {Id = 539}         -- "Tamara's House"
  1:  Cmp  {"QBits", Value = 301,   jump = 3}         -- NPC
  2:  Set  {"QBits", Value = 301}         -- NPC
  3:  Exit  {}
end

event 62
      Hint = str[28]  -- "Elixir of Revelation"
  0:  EnterHouse  {Id = 540}         -- "House"
end

event 75
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 76
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 77
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 78
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 79
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 90
  10: StatusText  {Str = 26}         -- "You need a key to enter the temple."
  11: Exit  {}

      Hint = str[191]
  0:  ForPlayer  ("All")
  1:  Cmp  {"QBits", Value = 426,   jump = 5}         -- Harbor Grace once.
  2:  Cmp  {"Inventory", Value = 525,   jump = 5}         -- "Harbor Grace Key"
  3:  StatusText  {Str = 26}         -- "You need a key to enter the temple."
  4:  Exit  {}

  5:  MoveToMap  {X = -15592, Y = 120, Z = -191, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 191, Icon = 5, Name = "T1.Blv"}         -- "Harbor Grace"
  6:  Exit  {}

  7:  EnterHouse  {Id = 191}         -- "Harbor Grace"
end

event 91
  0:  Cmp  {"QBits", Value = 301,   jump = 2}         -- NPC
  1:  Exit  {}

  2:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 1, Count = 3, X = -18160, Y = -1072, Z = 96}
  3:  Cmp  {"QBits", Value = 508,   jump = 6}         -- Warrior
  4:  MoveToMap  {X = -18176, Y = -1072, Z = 96, Direction = 512, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  5:  Exit  {}

  6:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 2, X = -18144, Y = -1079, Z = 96}
  7:  Cmp  {"QBits", Value = 507,   jump = 9}         -- Death Wish
  8:  GoTo  {jump = 4}

  9:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -18176, Y = -950, Z = 96}
  10: GoTo  {jump = 4}
end

event 92
  0:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 1, X = 200, Y = -14700, Z = -50}
  1:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 5, X = 200, Y = -14700, Z = -50}
  2:  Cmp  {"QBits", Value = 508,   jump = 5}         -- Warrior
  3:  MoveToMap  {X = 200, Y = -14600, Z = -50, Direction = 1024, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  4:  Exit  {}

  5:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 2, X = 190, Y = -14500, Z = -50}
  6:  Cmp  {"QBits", Value = 507,   jump = 8}         -- Death Wish
  7:  GoTo  {jump = 3}

  8:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 2, X = 210, Y = -14750, Z = -50}
  9:  GoTo  {jump = 3}
end

event 101
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 6}
  1:  Set  {"MapVar0", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 134, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar0", Value = 1,   jump = 4}
end

event 102
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 6}
  1:  Set  {"MapVar1", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 135, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar1", Value = 1,   jump = 4}
end

event 103
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 6}
  1:  Set  {"MapVar2", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 136, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar2", Value = 1,   jump = 4}
end

event 104
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 6}
  1:  Set  {"MapVar3", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 137, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar3", Value = 1,   jump = 4}
end

event 105
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar4", Value = 1,   jump = 6}
  1:  Set  {"MapVar4", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 138, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar4", Value = 1,   jump = 4}
end

event 106
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar5", Value = 1,   jump = 6}
  1:  Set  {"MapVar5", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 139, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar5", Value = 1,   jump = 4}
end

event 107
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar6", Value = 1,   jump = 6}
  1:  Set  {"MapVar6", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 140, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar6", Value = 1,   jump = 4}
end

event 108
      Hint = str[1]  -- "Tree"
  0:  Cmp  {"MapVar7", Value = 1,   jump = 6}
  1:  Set  {"MapVar7", Value = 1}
  2:  Add  {"Food", Value = 1}
  3:  StatusText  {Str = 1}         -- "Tree"
  4:  SetSprite  {SpriteId = 141, Visible = 1, Name = "Tree06"}
  5:  Exit  {}

  6:  StatusText  {Str = 2}         -- "There doesn't seem to be anymore apples."

  7:  OnLoadMap  {}
  8:  Cmp  {"MapVar7", Value = 1,   jump = 4}
end

event 109
      Hint = str[5]  -- "Drink from Fountain."
  0:  Cmp  {"MapVar9", Value = 1,   jump = 3}
  1:  StatusText  {Str = 7}         -- "Refreshing!"
  2:  Exit  {}

  3:  Subtract  {"MapVar9", Value = 1}
  4:  Add  {"SP", Value = 10}
  5:  StatusText  {Str = 8}         -- "+10 Spell points restored."
  6:  Set  {"AutonotesBits", Value = 11}         -- "10 Spell points restored by the central fountain in Ellesia."
  7:  Exit  {}

  8:  OnRefillTimer  {Second = 1}
  9:  Set  {"MapVar9", Value = 20}
end

event 110
      Hint = str[5]  -- "Drink from Fountain."
  0:  Cmp  {"IntellectBonus", Value = 10,   jump = 6}
  1:  Set  {"IntellectBonus", Value = 10}
  2:  Set  {"PersonalityBonus", Value = 10}
  3:  StatusText  {Str = 9}         -- "+10 Intellect and Personality temporary."
  4:  Set  {"AutonotesBits", Value = 13}         -- "10 Points of temporary intellect and personality from the west fountain at the Enchanted Bastion."
  5:  Exit  {}

  6:  StatusText  {Str = 7}         -- "Refreshing!"
  7:  Exit  {}
end

event 111
      Hint = str[5]  -- "Drink from Fountain."
  0:  Cmp  {"FireResBonus", Value = 5,   jump = 8}
  1:  Set  {"FireResBonus", Value = 5}
  2:  Set  {"ElecResBonus", Value = 5}
  3:  Set  {"ColdResBonus", Value = 5}
  4:  Set  {"PoisonResBonus", Value = 5}
  5:  StatusText  {Str = 10}         -- "+5 Elemental resistance temporary."
  6:  Set  {"AutonotesBits", Value = 14}         -- "5 Points of temporary fire, electricity, cold, and poison resistance from the east fountain at the Enchanted Bastion."
  7:  Exit  {}

  8:  StatusText  {Str = 7}         -- "Refreshing!"
  9:  Exit  {}
end

event 112
      Hint = str[4]  -- "Drink from Well."
  0:  Cmp  {"LuckBonus", Value = 20,   jump = 5}
  1:  Set  {"LuckBonus", Value = 20}
  2:  StatusText  {Str = 11}         -- "+20 Luck temporary."
  3:  Set  {"AutonotesBits", Value = 12}         -- "20 Points of temporary luck from the fountain west of the Imp Slapper in Ellesia."
  4:  Exit  {}

  5:  StatusText  {Str = 7}         -- "Refreshing!"
  6:  Exit  {}
end

event 113
      Hint = str[6]  -- "Drink from Trough."
  0:  DamagePlayer  {Player = "Current", DamageType = const.Damage.Cold, Damage = 20}
  1:  Set  {"PoisonedGreen", Value = 1}
  2:  StatusText  {Str = 12}         -- "Poison!"
end

event 114
      Hint = str[6]  -- "Drink from Trough."
  0:  StatusText  {Str = 7}         -- "Refreshing!"
end

event 115
      Hint = str[28]  -- "Elixir of Revelation"
  0:  Cmp  {"QBits", Value = 401,   jump = 4}         -- Elixir  of Revelation once
  1:  ForPlayer  ("All")
  2:  Set  {"PerceptionSkill", Value = 68}
  3:  Add  {"QBits", Value = 401}         -- Elixir  of Revelation once
  4:  Exit  {}
end

event 210
  0:  OnTimer  {IntervalInHalfMinutes = 10}
  1:  Cmp  {"QBits", Value = 157,   jump = 3}         -- NPC
  2:  Cmp  {"Flying", Value = 0,   jump = 4}
  3:  Exit  {}

  4:  CastSpell  {Spell = 6, Mastery = const.Master, Skill = 5, FromX = 3039, FromY = -9201, FromZ = 2818, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 211
  0:  ForPlayer  ("All")
  1:  Cmp  {"QBits", Value = 157,   jump = 3}         -- NPC
  2:  Cmp  {"Inventory", Value = 486,   jump = 4}         -- "Dragon Tower Keys"
  3:  Exit  {}

  4:  Set  {"QBits", Value = 157}         -- NPC
  5:  SetTextureOutdoors  {Model = 53, Facet = 42, Name = "T1swBu"}
end

event 212
      Hint = str[20]  -- "Obelisk"
  0:  SetMessage  {Str = 19}         -- "The surface of the obelisk is blood warm to the touch.  A message swims into view as you remove your hand:                                                                                                                                                           iNs_u_t_rfesh_'ns"
  1:  SimpleMessage  {}
  2:  Set  {"QBits", Value = 373}         -- NPC
  3:  Set  {"AutonotesBits", Value = 92}         -- "Obelisk Message # 14: iNs_u_t_rfesh_'ns"
end

event 213
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 157,   jump = 3}         -- NPC
  2:  Exit  {}

  3:  SetTextureOutdoors  {Model = 53, Facet = 42, Name = "T1swBu"}
end

event 261
      Hint = str[15]  -- "Shrine of Might"
  0:  Cmp  {"MonthIs", Value = 0,   jump = 3}
  1:  StatusText  {Str = 16}         -- "You pray at the shrine."
  2:  Exit  {}

  3:  Cmp  {"QBits", Value = 206,   jump = 1}         -- NPC
  4:  Set  {"QBits", Value = 206}         -- NPC
  5:  Cmp  {"QBits", Value = 208,   jump = 11}         -- NPC
  6:  Set  {"QBits", Value = 208}         -- NPC
  7:  ForPlayer  ("All")
  8:  Add  {"BaseMight", Value = 10}
  9:  StatusText  {Str = 17}         -- "+10 Might permanent"
  10: Exit  {}

  11: ForPlayer  ("All")
  12: Add  {"BaseMight", Value = 3}
  13: StatusText  {Str = 18}         -- "+3 Might permanent"
end

event 262
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 505,   jump = 3}         -- Misty Reload
  2:  Cmp  {"QBits", Value = 508,   jump = 4}         -- Warrior
  3:  Exit  {}

  4:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 10, X = 9550, Y = 10200, Z = 0}
  5:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 2, X = 9555, Y = 10220, Z = 0}
  6:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 10, X = 10100, Y = 11400, Z = 0}
  7:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 2, X = 10200, Y = 11400, Z = 0}
  8:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 10, X = 5450, Y = 17050, Z = 0}
  9:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = 5460, Y = 17050, Z = 0}
  10: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 10, X = -8166, Y = -7000, Z = 0}
  11: SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -8155, Y = -6990, Z = 0}
  12: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 5, X = 13700, Y = 8450, Z = 0}
  13: SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 4, X = -4670, Y = -18620, Z = 0}
  14: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 7, X = -4670, Y = -18620, Z = 0}
  15: SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 4, X = -7960, Y = -12220, Z = 0}
  16: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 10, X = -7955, Y = -12230, Z = 0}
  17: Cmp  {"QBits", Value = 507,   jump = 20}         -- Death Wish
  18: Set  {"QBits", Value = 505}         -- Misty Reload
  19: Exit  {}

  20: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = -7950, Y = -12210, Z = 0}
  21: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 5, X = -265, Y = 11000, Z = 0}
  22: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 5, X = -11765, Y = 6880, Z = 0}
  23: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = -9130, Y = 12060, Z = 0}
  24: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 5, X = -5630, Y = -6620, Z = 0}
  25: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 5, X = -5125, Y = -6583, Z = 0}
  26: Set  {"QBits", Value = 505}         -- Misty Reload
  27: Exit  {}

  28: Subtract  {"QBits", Value = 505}         -- Misty Reload
  29: Exit  {}
end

event 263
  0:  Subtract  {"HasFullSP", Value = 0}
end
