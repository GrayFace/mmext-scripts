str[0] = " "
str[1] = "Door"
str[2] = "Exit"
str[3] = "Chest"
str[4] = "Sign"
str[5] = "Door "
str[6] = "There is a hissing sound coming from the wall"
str[7] = "The crimson embers will lead the way"
str[8] = "Lever"
str[9] = "The way has been cleared"
str[10] = "The fires of the dead shall burn forever"
str[11] = "Podium"
str[12] = "The Book is destroyed"
str[13] = "The Book of Liches is destroyed"
str[14] = "Crystal"
str[15] = "Forcefield"
str[16] = "Your way is blocked."
str[17] = "Temple of Tranquility"
str[18] = "Sarcophagus"
str[19] = "How Clever!  +20 Skill points"
str[20] = "Steal from the dead?"
str[21] = "Steal (Yes/No)?"
str[22] = "Yes"
str[23] = "Y"
str[24] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[17]  -- "Temple of Tranquility"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 9, State = 1}
end

event 10
      Hint = str[3]  -- "Chest"
  0:  OpenChest  {Id = 0}
end

event 2
  0:  CastSpell  {Spell = 90, Mastery = const.Novice, Skill = 10, FromX = 13819, FromY = -866, FromZ = -180, ToX = 0, ToY = 0, ToZ = 0}         -- "Toxic Cloud"
end

event 3
  0:  CastSpell  {Spell = 32, Mastery = const.Novice, Skill = 8, FromX = 11136, FromY = 3712, FromZ = -80, ToX = 0, ToY = 0, ToZ = 0}         -- "Ice Blast"
end

event 19
  0:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 10, FromX = 10417, FromY = 4800, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  1:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 10, FromX = 10706, FromY = 2258, FromZ = 150, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  2:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 10, FromX = 10706, FromY = 1628, FromZ = 150, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  3:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 10, FromX = 9978, FromY = 1914, FromZ = 150, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 20
      Hint = str[5]  -- "Door "
  0:  CastSpell  {Spell = 90, Mastery = const.Novice, Skill = 1, FromX = 14925, FromY = 2518, FromZ = -689, ToX = 0, ToY = 0, ToZ = 0}         -- "Toxic Cloud"
end

event 21
  0:  CastSpell  {Spell = 28, Mastery = const.Novice, Skill = 1, FromX = 15217, FromY = 576, FromZ = 528, ToX = 0, ToY = 0, ToZ = 0}         -- "Ice Bolt"
  1:  CastSpell  {Spell = 28, Mastery = const.Novice, Skill = 1, FromX = 15112, FromY = 171, FromZ = 529, ToX = 0, ToY = 0, ToZ = 0}         -- "Ice Bolt"
  2:  CastSpell  {Spell = 28, Mastery = const.Novice, Skill = 1, FromX = 15123, FromY = 405, FromZ = 529, ToX = 0, ToY = 0, ToZ = 0}         -- "Ice Bolt"
  3:  Exit  {}
end

event 22
  0:  CastSpell  {Spell = 28, Mastery = const.Novice, Skill = 1, FromX = 15217, FromY = 576, FromZ = 528, ToX = 0, ToY = 0, ToZ = 0}         -- "Ice Bolt"
  1:  CastSpell  {Spell = 28, Mastery = const.Novice, Skill = 1, FromX = 15112, FromY = 171, FromZ = 529, ToX = 0, ToY = 0, ToZ = 0}         -- "Ice Bolt"
  2:  CastSpell  {Spell = 28, Mastery = const.Novice, Skill = 1, FromX = 15123, FromY = 405, FromZ = 529, ToX = 0, ToY = 0, ToZ = 0}         -- "Ice Bolt"
end

event 23
  0:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 14718, FromY = 2456, FromZ = 541, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 24
  0:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 18915, FromY = 2035, FromZ = 541, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 25
  0:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 18111, FromY = 10127, FromZ = 386, ToX = 18111, ToY = 4782, ToZ = 386}         -- "Fireball"
  1:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 18131, FromY = 10127, FromZ = 386, ToX = 18131, ToY = 4782, ToZ = 386}         -- "Fireball"
  2:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 18151, FromY = 10127, FromZ = 386, ToX = 18151, ToY = 4782, ToZ = 386}         -- "Fireball"
  3:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 18171, FromY = 10127, FromZ = 386, ToX = 18171, ToY = 4782, ToZ = 386}         -- "Fireball"
  4:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 18191, FromY = 10127, FromZ = 386, ToX = 18191, ToY = 4782, ToZ = 386}         -- "Fireball"
  5:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 18201, FromY = 10127, FromZ = 386, ToX = 18201, ToY = 4782, ToZ = 386}         -- "Fireball"
end

event 26
  0:  Cmp  {"QBits", Value = 9,   jump = 2}         --  9, CD2, given when you destroy Lich book
  1:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -7522, Y = 14848, Z = -240}
  2:  Exit  {}
end

event 27
  0:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -2904, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  1:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -2432, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  2:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -1960, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  3:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -1606, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  4:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -1134, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  5:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -426, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  6:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -72, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  7:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 400, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  8:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 1108, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  9:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 1462, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  10: CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 1934, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  11: CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 2642, FromY = 16512, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 28
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  MoveToMap  {X = 22768, Y = 7504, Z = 1170, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  SetDoorState  {Id = 10, State = 1}
  4:  StatusText  {Str = 9}         -- "The way has been cleared"
end

event 29
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  MoveToMap  {X = 10384, Y = 2224, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  SetDoorState  {Id = 10, State = 1}
  4:  StatusText  {Str = 9}         -- "The way has been cleared"
end

event 30
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  MoveToMap  {X = 22768, Y = 7504, Z = 1170, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  SetDoorState  {Id = 10, State = 1}
  4:  StatusText  {Str = 9}         -- "The way has been cleared"
end

event 31
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  MoveToMap  {X = 8608, Y = 128, Z = 630, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  SetDoorState  {Id = 10, State = 1}
  4:  StatusText  {Str = 9}         -- "The way has been cleared"
end

event 32
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  MoveToMap  {X = 22768, Y = 7504, Z = 1170, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  SetDoorState  {Id = 10, State = 1}
  4:  StatusText  {Str = 9}         -- "The way has been cleared"
end

event 33
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  MoveToMap  {X = 2560, Y = 3856, Z = -636, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  Cmp  {"MapVar1", Value = 2,   jump = 1}
  4:  Add  {"MapVar1", Value = 1}
  5:  SetFacetBit  {Id = 4522, Bit = const.FacetBits.Untouchable, On = true}
  6:  SetFacetBit  {Id = 4575, Bit = const.FacetBits.Untouchable, On = true}
  7:  StatusText  {Str = 9}         -- "The way has been cleared"
  8:  Exit  {}
end

event 35
  0:  Cmp  {"MapVar1", Value = 1,   jump = 4}
  1:  SummonObject  {Type = 2081, X = 317, Y = 14144, Z = 191, Speed = 1000, Count = 1, RandomAngle = false}         -- explosion
  2:  SummonObject  {Type = 1, X = 317, Y = 14144, Z = 320, Speed = 10, Count = 1, RandomAngle = false}         -- long sword
  3:  Exit  {}

  4:  Cmp  {"MapVar1", Value = 2,   jump = 9}
  5:  Add  {"MapVar1", Value = 1}
  6:  SetFacetBit  {Id = 4522, Bit = const.FacetBits.Untouchable, On = true}
  7:  SetFacetBit  {Id = 4575, Bit = const.FacetBits.Untouchable, On = true}
  8:  StatusText  {Str = 9}         -- "The way has been cleared"
  9:  Exit  {}
end

event 36
  0:  Cmp  {"MapVar1", Value = 1,   jump = 4}
  1:  SummonObject  {Type = 2081, X = 551, Y = 14144, Z = 191, Speed = 1000, Count = 1, RandomAngle = false}         -- explosion
  2:  SummonObject  {Type = 1, X = 551, Y = 14144, Z = 320, Speed = 10, Count = 1, RandomAngle = false}         -- long sword
  3:  Exit  {}

  4:  Cmp  {"MapVar1", Value = 2,   jump = 9}
  5:  Add  {"MapVar1", Value = 1}
  6:  SetFacetBit  {Id = 4522, Bit = const.FacetBits.Untouchable, On = true}
  7:  SetFacetBit  {Id = 4575, Bit = const.FacetBits.Untouchable, On = true}
  8:  StatusText  {Str = 9}         -- "The way has been cleared"
  9:  Exit  {}
end

event 37
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  MoveToMap  {X = 16080, Y = 9072, Z = -180, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  Cmp  {"MapVar1", Value = 2,   jump = 1}
  4:  Add  {"MapVar1", Value = 1}
  5:  SetFacetBit  {Id = 4522, Bit = const.FacetBits.Untouchable, On = true}
  6:  SetFacetBit  {Id = 4575, Bit = const.FacetBits.Untouchable, On = true}
  7:  StatusText  {Str = 9}         -- "The way has been cleared"
  8:  Exit  {}
end

event 38
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  MoveToMap  {X = -10240, Y = 12144, Z = -240, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  MoveToMap  {X = 22080, Y = -2192, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 39
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  MoveToMap  {X = -7328, Y = 10496, Z = 600, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  MoveToMap  {X = 22080, Y = -2192, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 40
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  MoveToMap  {X = -6112, Y = 10912, Z = 600, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  MoveToMap  {X = 22080, Y = -2192, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 41
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  MoveToMap  {X = -10240, Y = 12144, Z = -240, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  MoveToMap  {X = 22080, Y = -2192, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 42
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  MoveToMap  {X = -10240, Y = 12144, Z = -240, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
  2:  Exit  {}

  3:  MoveToMap  {X = 22080, Y = -2192, Z = 1, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 43
  0:  MoveToMap  {X = 13744, Y = 640, Z = -180, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 44
  0:  MoveToMap  {X = 2528, Y = 3568, Z = -635, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 45
      Hint = str[4]  -- "Sign"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 15}
  1:  Set  {"MapVar2", Value = 1}
  2:  SetMessage  {Str = 10}         -- "The fires of the dead shall burn forever"
  3:  SimpleMessage  {}
  4:  SetTexture  {Facet = 4298, Name = "lavatyl"}
  5:  SetTexture  {Facet = 4299, Name = "lavatyl"}
  6:  SetTexture  {Facet = 4300, Name = "lavatyl"}
  7:  SetTexture  {Facet = 4301, Name = "lavatyl"}
  8:  SetTexture  {Facet = 4302, Name = "lavatyl"}
  9:  SetFacetBit  {Id = 4298, Bit = const.FacetBits.IsWater, On = true}
  10: SetFacetBit  {Id = 4299, Bit = const.FacetBits.IsWater, On = true}
  11: SetFacetBit  {Id = 4300, Bit = const.FacetBits.IsWater, On = true}
  12: SetFacetBit  {Id = 4301, Bit = const.FacetBits.IsWater, On = true}
  13: SetFacetBit  {Id = 4302, Bit = const.FacetBits.IsWater, On = true}
  14: Exit  {}

  15: Set  {"MapVar2", Value = 0}
  16: SetTexture  {Facet = 4298, Name = "orwtrtyl"}
  17: SetTexture  {Facet = 4299, Name = "orwtrtyl"}
  18: SetTexture  {Facet = 4300, Name = "orwtrtyl"}
  19: SetTexture  {Facet = 4301, Name = "orwtrtyl"}
  20: SetTexture  {Facet = 4302, Name = "orwtrtyl"}
end

event 46
  0:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -2904, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  1:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -2432, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  2:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -1960, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  3:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -1606, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  4:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -1134, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  5:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -426, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  6:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = -72, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  7:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 400, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  8:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 1108, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  9:  CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 1462, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  10: CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 1934, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
  11: CastSpell  {Spell = 6, Mastery = const.Novice, Skill = 1, FromX = 2642, FromY = 11904, FromZ = 100, ToX = 0, ToY = 0, ToZ = 0}         -- "Fireball"
end

event 47
  0:  Cmp  {"QBits", Value = 9,   jump = 2}         --  9, CD2, given when you destroy Lich book
  1:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -6144, Y = 14720, Z = -240}
  2:  Exit  {}
end

event 48
  0:  Cmp  {"QBits", Value = 9,   jump = 2}         --  9, CD2, given when you destroy Lich book
  1:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -5120, Y = 14208, Z = -240}
  2:  Exit  {}
end

event 49
  0:  Cmp  {"QBits", Value = 9,   jump = 2}         --  9, CD2, given when you destroy Lich book
  1:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -5760, Y = 12800, Z = -240}
  2:  Exit  {}
end

event 50
  0:  Cmp  {"QBits", Value = 9,   jump = 2}         --  9, CD2, given when you destroy Lich book
  1:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -7552, Y = 12800, Z = -240}
  2:  Exit  {}
end

event 51
  0:  Cmp  {"QBits", Value = 9,   jump = 2}         --  9, CD2, given when you destroy Lich book
  1:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -7808, Y = 13056, Z = -240}
  2:  Exit  {}
end

event 52
  0:  Cmp  {"QBits", Value = 9,   jump = 2}         --  9, CD2, given when you destroy Lich book
  1:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -5376, Y = 11904, Z = -240}
  2:  Exit  {}
end

event 53
      Hint = str[4]  -- "Sign"
  0:  SetMessage  {Str = 7}         -- "The crimson embers will lead the way"
  1:  SimpleMessage  {}
  2:  SetDoorState  {Id = 13, State = 2}         -- switch state
end

event 54
      Hint = str[8]  -- "Lever"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 14}
  1:  Set  {"MapVar0", Value = 1}
  2:  SetDoorState  {Id = 14, State = 2}         -- switch state
  3:  SetTexture  {Facet = 4219, Name = "lavatyl"}
  4:  SetTexture  {Facet = 4220, Name = "lavatyl"}
  5:  SetTexture  {Facet = 4221, Name = "lavatyl"}
  6:  SetTexture  {Facet = 4222, Name = "lavatyl"}
  7:  SetTexture  {Facet = 4223, Name = "lavatyl"}
  8:  SetFacetBit  {Id = 4219, Bit = const.FacetBits.IsWater, On = true}
  9:  SetFacetBit  {Id = 4220, Bit = const.FacetBits.IsWater, On = true}
  10: SetFacetBit  {Id = 4221, Bit = const.FacetBits.IsWater, On = true}
  11: SetFacetBit  {Id = 4222, Bit = const.FacetBits.IsWater, On = true}
  12: SetFacetBit  {Id = 4223, Bit = const.FacetBits.IsWater, On = true}
  13: Exit  {}

  14: SetDoorState  {Id = 14, State = 2}         -- switch state
  15: Set  {"MapVar0", Value = 0}
  16: SetTexture  {Facet = 4219, Name = "orwtrtyl"}
  17: SetTexture  {Facet = 4220, Name = "orwtrtyl"}
  18: SetTexture  {Facet = 4221, Name = "orwtrtyl"}
  19: SetTexture  {Facet = 4222, Name = "orwtrtyl"}
  20: SetTexture  {Facet = 4223, Name = "orwtrtyl"}
end

event 55
      Hint = str[4]  -- "Sign"
  0:  SetMessage  {Str = 7}         -- "The crimson embers will lead the way"
  1:  SimpleMessage  {}
  2:  SetDoorState  {Id = 11, State = 2}         -- switch state
end

event 56
      Hint = str[4]  -- "Sign"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 14}
  1:  SetDoorState  {Id = 12, State = 2}         -- switch state
  2:  Set  {"MapVar1", Value = 1}
  3:  SetTexture  {Facet = 4265, Name = "lavatyl"}
  4:  SetTexture  {Facet = 4266, Name = "lavatyl"}
  5:  SetTexture  {Facet = 4267, Name = "lavatyl"}
  6:  SetTexture  {Facet = 4268, Name = "lavatyl"}
  7:  SetTexture  {Facet = 4269, Name = "lavatyl"}
  8:  SetFacetBit  {Id = 4265, Bit = const.FacetBits.IsWater, On = true}
  9:  SetFacetBit  {Id = 4266, Bit = const.FacetBits.IsWater, On = true}
  10: SetFacetBit  {Id = 4267, Bit = const.FacetBits.IsWater, On = true}
  11: SetFacetBit  {Id = 4268, Bit = const.FacetBits.IsWater, On = true}
  12: SetFacetBit  {Id = 4269, Bit = const.FacetBits.IsWater, On = true}
  13: Exit  {}

  14: SetDoorState  {Id = 14, State = 2}         -- switch state
  15: Set  {"MapVar1", Value = 0}
  16: SetTexture  {Facet = 4265, Name = "orwtrtyl"}
  17: SetTexture  {Facet = 4266, Name = "orwtrtyl"}
  18: SetTexture  {Facet = 4267, Name = "orwtrtyl"}
  19: SetTexture  {Facet = 4269, Name = "orwtrtyl"}
  20: SetTexture  {Facet = 4269, Name = "orwtrtyl"}
end

event 34
  0:  Exit  {}

  10: Hint  {Str = 11}         -- "Podium"
  10: Cmp  {"QBits", Value = 9,   jump = 5}         --  9, CD2, given when you destroy Lich book
  11: StatusText  {Str = 13}         -- "The Book of Liches is destroyed"
  12: Add  {"QBits", Value = 9}         --  9, CD2, given when you destroy Lich book
  13: SetTexture  {Facet = 4560, Name = "deskside"}
  14: Exit  {}

  15: StatusText  {Str = 12}         -- "The Book is destroyed"
  6:  Exit  {}
end

event 57
      Hint = str[14]  -- "Crystal"
  0:  ForPlayer  ("All")
  1:  Cmp  {"QBits", Value = 102,   jump = 7}         -- Oracle
  2:  Cmp  {"Inventory", Value = 552,   jump = 7}         -- "Urn #4"
  3:  SetSprite  {SpriteId = 329, Visible = 1, Name = "crysdisc"}
  4:  ForPlayer  ("Random")
  5:  Add  {"Inventory", Value = 552}         -- "Urn #4"
  6:  Set  {"QBits", Value = 193}         -- Quest item bits for seer
  7:  Exit  {}
end

event 58
  0:  OnLoadMap  {}
  1:  ForPlayer  ("All")
  2:  Cmp  {"QBits", Value = 102,   jump = 5}         -- Oracle
  3:  Cmp  {"Inventory", Value = 552,   jump = 5}         -- "Urn #4"
  4:  GoTo  {jump = 6}

  5:  SetSprite  {SpriteId = 329, Visible = 1, Name = "crysdisc"}
  6:  Cmp  {"MapVar2", Value = 1,   jump = 8}
  7:  GoTo  {jump = 13}

  8:  SetTexture  {Facet = 4298, Name = "lavatyl"}
  9:  SetTexture  {Facet = 4299, Name = "lavatyl"}
  10: SetTexture  {Facet = 4300, Name = "lavatyl"}
  11: SetTexture  {Facet = 4301, Name = "lavatyl"}
  12: SetTexture  {Facet = 4302, Name = "lavatyl"}
  13: Cmp  {"MapVar0", Value = 1,   jump = 15}
  14: GoTo  {jump = 20}

  15: SetTexture  {Facet = 4219, Name = "lavatyl"}
  16: SetTexture  {Facet = 4220, Name = "lavatyl"}
  17: SetTexture  {Facet = 4221, Name = "lavatyl"}
  18: SetTexture  {Facet = 4222, Name = "lavatyl"}
  19: SetTexture  {Facet = 4223, Name = "lavatyl"}
  20: Cmp  {"MapVar1", Value = 1,   jump = 22}
  21: GoTo  {jump = 27}

  22: SetTexture  {Facet = 4265, Name = "lavatyl"}
  23: SetTexture  {Facet = 4266, Name = "lavatyl"}
  24: SetTexture  {Facet = 4267, Name = "lavatyl"}
  25: SetTexture  {Facet = 4268, Name = "lavatyl"}
  26: SetTexture  {Facet = 4269, Name = "lavatyl"}
  27: Cmp  {"QBits", Value = 9,   jump = 29}         --  9, CD2, given when you destroy Lich book
  28: GoTo  {jump = 30}

  29: SetTexture  {Facet = 4560, Name = "deskside"}
  30: Cmp  {"MapVar1", Value = 1,   jump = 32}
  31: Exit  {}

  32: SetFacetBit  {Id = 4522, Bit = const.FacetBits.Untouchable, On = true}
  33: SetFacetBit  {Id = 4575, Bit = const.FacetBits.Untouchable, On = true}
end

event 59
      Hint = str[15]  -- "Forcefield"
  0:  StatusText  {Str = 16}         -- "Your way is blocked."
end

event 60
  0:  MoveToMap  {X = 11339, Y = -3106, Z = 97, Direction = 56, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutC1.Odm"}
end

event 61
      Hint = str[18]  -- "Sarcophagus"
  0:  Cmp  {"MapVar9", Value = 1,   jump = 3}
  1:  SetMessage  {Str = 20}         -- "Steal from the dead?"
  2:  Question  {Question = 21, Answer1 = 22, Answer2 = 23,   jump(ok) = 4}         -- "Steal (Yes/No)?" ("Yes", "Y")
  3:  Exit  {}

  4:  Set  {"MapVar9", Value = 1}
  5:  GiveItem  {Strength = 6, Type = const.ItemType.Helm_, Id = 0}
  6:  Subtract  {"ReputationIs", Value = 200}
end

event 62
      Hint = str[18]  -- "Sarcophagus"
  0:  Cmp  {"MapVar10", Value = 1,   jump = 3}
  1:  SetMessage  {Str = 20}         -- "Steal from the dead?"
  2:  Question  {Question = 21, Answer1 = 22, Answer2 = 23,   jump(ok) = 4}         -- "Steal (Yes/No)?" ("Yes", "Y")
  3:  Exit  {}

  4:  Set  {"MapVar10", Value = 1}
  5:  GiveItem  {Strength = 6, Type = const.ItemType.Boots_, Id = 0}
  6:  Subtract  {"ReputationIs", Value = 200}
end

event 63
      Hint = str[18]  -- "Sarcophagus"
  0:  Cmp  {"MapVar11", Value = 1,   jump = 3}
  1:  SetMessage  {Str = 20}         -- "Steal from the dead?"
  2:  Question  {Question = 21, Answer1 = 22, Answer2 = 23,   jump(ok) = 4}         -- "Steal (Yes/No)?" ("Yes", "Y")
  3:  Exit  {}

  4:  Set  {"MapVar11", Value = 1}
  5:  GiveItem  {Strength = 6, Type = const.ItemType.Belt_, Id = 0}
  6:  Subtract  {"ReputationIs", Value = 200}
end

event 64
  0:  Cmp  {"MapVar12", Value = 1,   jump = 5}
  1:  ForPlayer  ("Current")
  2:  Set  {"MapVar12", Value = 1}
  3:  Add  {"SkillPoints", Value = 20}
  4:  StatusText  {Str = 19}         -- "How Clever!  +20 Skill points"
  5:  Exit  {}
end

event 75
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 462,   jump = 6}         -- Temple of Tranquility Once
  2:  Set  {"QBits", Value = 462}         -- Temple of Tranquility Once
  3:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 1, Count = 4, X = 14313, Y = 7307, Z = -179}
  4:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 2, X = 14300, Y = 7300, Z = -179}
  5:  Cmp  {"QBits", Value = 508,   jump = 7}         -- Warrior
  6:  Exit  {}

  7:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = 14238, Y = 6401, Z = -179}
  8:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = 11927, Y = 4156, Z = -179}
  9:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = 15387, Y = 3496, Z = -179}
  10: SummonMonsters  {TypeIndexInMapStats = 1, Level = 1, Count = 6, X = 11677, Y = 12784, Z = -179}
  11: Cmp  {"QBits", Value = 507,   jump = 13}         -- Death Wish
  12: Exit  {}

  13: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 7, X = 13221, Y = 6034, Z = -179}
  14: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 7, X = 12473, Y = 3664, Z = -179}
  15: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 7, X = 9409, Y = 12595, Z = -179}
  16: Exit  {}
end
