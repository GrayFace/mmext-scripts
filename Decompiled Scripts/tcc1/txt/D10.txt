str[0] = " "
str[1] = "Door"
str[2] = "Chest"
str[3] = "Lever"
str[4] = "Cabinet"
str[5] = "Switch"
str[6] = "The Door won't budge."
str[7] = "Caught!"
str[8] = "Guards!"
str[9] = "Are those footsteps?"
str[10] = "Exit"
str[11] = "Druid's Temple"
str[12] = "You open the chest and exchange the pearls."
str[13] = "The chest is locked."
str[14] = ""
str[15] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[11]  -- "Druid's Temple"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 3, State = 1}
end

event 4
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 4, State = 1}
end

event 5
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 5, State = 1}
end

event 6
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 6, State = 1}
end

event 7
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 7, State = 1}
end

event 8
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 8, State = 1}
end

event 9
      Hint = str[3]  -- "Lever"
  0:  SetDoorState  {Id = 9, State = 2}         -- switch state
  1:  SetDoorState  {Id = 28, State = 2}         -- switch state
end

event 10
      Hint = str[3]  -- "Lever"
  0:  SetDoorState  {Id = 10, State = 1}
  1:  SetDoorState  {Id = 29, State = 2}         -- switch state
end

event 11
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 11, State = 1}
end

event 12
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 12, State = 1}
end

event 13
  0:  SetDoorState  {Id = 13, State = 1}
end

event 14
      Hint = str[5]  -- "Switch"
  0:  SetDoorState  {Id = 14, State = 2}         -- switch state
  1:  SetDoorState  {Id = 15, State = 2}         -- switch state
end

event 16
      Hint = str[5]  -- "Switch"
  0:  SetDoorState  {Id = 16, State = 2}         -- switch state
  1:  SetDoorState  {Id = 17, State = 2}         -- switch state
end

event 18
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 18, State = 1}
end

event 19
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 19, State = 1}
end

event 20
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 20, State = 1}
end

event 21
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 21, State = 1}
end

event 22
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 22, State = 1}
end

event 23
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 23, State = 1}
end

event 24
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 24, State = 1}
end

event 25
      Hint = str[1]  -- "Door"
  0:  SetDoorState  {Id = 25, State = 1}
end

event 26
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 27
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 28
      Hint = str[2]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 34
      Hint = str[4]  -- "Cabinet"
  0:  OpenChest  {Id = 4}
end

event 35
      Hint = str[4]  -- "Cabinet"
  0:  OpenChest  {Id = 5}
end

event 36
      Hint = str[4]  -- "Cabinet"
  0:  OpenChest  {Id = 6}
end

event 37
      Hint = str[4]  -- "Cabinet"
  0:  OpenChest  {Id = 7}
end

event 38
      Hint = str[4]  -- "Cabinet"
  0:  OpenChest  {Id = 8}
end

event 39
      Hint = str[4]  -- "Cabinet"
  0:  OpenChest  {Id = 9}
end

event 29
      Hint = str[2]  -- "Chest"
  0:  Cmp  {"QBits", Value = 4,   jump = 8}         --  4 D10, given when you retreive artifact from chest
  1:  Set  {"QBits", Value = 4}         --  4 D10, given when you retreive artifact from chest
  2:  StatusText  {Str = 12}         -- "You open the chest and exchange the pearls."
  3:  ForPlayer  ("All")
  4:  Subtract  {"Inventory", Value = 458}         -- "Pearl of Turmoil"
  5:  ForPlayer  ("Current")
  6:  Add  {"Inventory", Value = 459}         -- "Pearl of Fertility"
  7:  Exit  {}

  8:  StatusText  {Str = 13}         -- "The chest is locked."
  9:  Exit  {}

  12: OpenChest  {Id = 10}
  13: Exit  {}

  14: OpenChest  {Id = 11}
end

event 30
      Hint = str[3]  -- "Lever"
  0:  SetDoorState  {Id = 26, State = 2}         -- switch state
  1:  Cmp  {"MapVar0", Value = 5,   jump = 5}
  2:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 2, Count = 3, X = -2976, Y = 2512, Z = 0}
  3:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 1, X = -2974, Y = 1510, Z = 0}
  4:  Add  {"MapVar0", Value = 1}
  5:  Exit  {}
end

event 31
      Hint = str[3]  -- "Lever"
  0:  SetDoorState  {Id = 27, State = 2}         -- switch state
  1:  Cmp  {"MapVar0", Value = 5,   jump = 5}
  2:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 1, Count = 3, X = -1111, Y = 4424, Z = 0}
  3:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 2, Count = 2, X = -1100, Y = 4420, Z = 0}
  4:  Add  {"MapVar0", Value = 1}
  5:  Exit  {}
end

event 33
      Hint = str[10]  -- "Exit"
  0:  MoveToMap  {X = -17658, Y = -12361, Z = 257, Direction = 1024, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutD1.Odm"}
end

event 32
      Hint = str[1]  -- "Door"
  0:  StatusText  {Str = 6}         -- "The Door won't budge."
end

event 40
  0:  Cmp  {"MapVar1", Value = 1,   jump = 7}
  1:  SetDoorState  {Id = 17, State = 0}
  2:  FaceExpression  {Player = "All", Frame = 5}
  3:  SetSprite  {SpriteId = 69, Visible = 1, Name = "torchnf"}
  4:  StatusText  {Str = 7}         -- "Caught!"
  5:  Set  {"MapVar1", Value = 1}
  6:  Set  {"MapVar2", Value = 1}
  7:  Exit  {}
end

event 41
  0:  OnTimer  {IntervalInHalfMinutes = 15}
  1:  Cmp  {"MapVar4", Value = 1,   jump = 8}
  2:  Cmp  {"MapVar2", Value = 1,   jump = 4}
  3:  Exit  {}

  4:  Cmp  {"MapVar2", Value = 3,   jump = 9}
  5:  StatusText  {Str = 8}         -- "Guards!"
  6:  SummonMonsters  {TypeIndexInMapStats = 3, Level = 2, Count = 3, X = -1216, Y = 10992, Z = -256}
  7:  Add  {"MapVar2", Value = 1}
  8:  Exit  {}

  9:  StatusText  {Str = 9}         -- "Are those footsteps?"
  10: SummonMonsters  {TypeIndexInMapStats = 3, Level = 3, Count = 3, X = -1152, Y = 9840, Z = -256}
  11: SetDoorState  {Id = 17, State = 1}
  12: Set  {"MapVar4", Value = 1}
end

event 42
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 4,   jump = 3}         --  4 D10, given when you retreive artifact from chest
  2:  Exit  {}

  3:  Set  {"MapVar3", Value = 1}
end

event 43
  0:  Cmp  {"MapVar5", Value = 1,   jump = 3}
  1:  GiveItem  {Strength = 5, Type = const.ItemType.Sword, Id = 0}
  2:  Set  {"MapVar5", Value = 1}
  3:  Exit  {}
end
