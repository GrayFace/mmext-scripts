str[0] = " "
str[1] = "Door"
str[2] = "Cabinet"
str[3] = "Crystal of Accuracy"
str[4] = "Full sack"
str[5] = "Switch"
str[6] = "Flickering Torch"
str[7] = "Empty barrel"
str[8] = "The skulls seem to sap your might"
str[9] = "You have restored the Crystal of Accuracy."
str[10] = "You pull the torch and it shifts in your hand"
str[11] = "The door won't budge"
str[12] = "(removed) The keg is empty"
str[13] = "Exit"
str[14] = "(removed) Keg"
str[15] = "Wrenford's Retreat"
str[16] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[15]  -- "Wrenford's Retreat"
  0:  SetDoorState  {Id = 1, State = 1}
end

event 2
      Hint = str[1]  -- "Door"
  0:  Exit  {}
end

event 3
      Hint = str[6]  -- "Flickering Torch"
  0:  SetDoorState  {Id = 3, State = 1}
  1:  StatusText  {Str = 10}         -- "You pull the torch and it shifts in your hand"
end

event 4
      Hint = str[5]  -- "Switch"
  0:  SetDoorState  {Id = 4, State = 1}
  1:  SetDoorState  {Id = 2, State = 1}
end

event 5
      Hint = str[2]  -- "Cabinet"
  0:  OpenChest  {Id = 0}
end

event 6
      Hint = str[2]  -- "Cabinet"
  0:  OpenChest  {Id = 1}
end

event 7
      Hint = str[4]  -- "Full sack"
  0:  OpenChest  {Id = 2}
end

event 8
  0:  ForPlayer  ("All")
  1:  Subtract  {"MightBonus", Value = 10}
  2:  StatusText  {Str = 8}         -- "The skulls seem to sap your might"
end

event 9
      Hint = str[3]  -- "Crystal of Accuracy"
  0:  Cmp  {"QBits", Value = 473,   jump = 5}         -- Accuracy Crystal once
  1:  ForPlayer  ("All")
  2:  Add  {"BaseAccuracy", Value = 20}
  3:  StatusText  {Str = 9}         -- "You have restored the Crystal of Accuracy."
  4:  Set  {"QBits", Value = 473}         -- Accuracy Crystal once
  5:  Exit  {}
end

event 10
      Hint = str[1]  -- "Door"
  0:  StatusText  {Str = 11}         -- "The door won't budge"
end

event 11
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 21,   jump = 3}         -- 21 T2, Given when evil crystal is destroyed
  2:  Exit  {}

  3:  SetSprite  {SpriteId = 25, Visible = 0, Name = "0"}
end

event 50
      Hint = str[13]  -- "Exit"
  0:  MoveToMap  {X = -21001, Y = -4653, Z = 257, Direction = 1536, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutD2.Odm"}
end

event 55
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  Set  {"MapVar0", Value = 1}
  2:  Add  {"Inventory", Value = 275}         -- "Flying Fist"
  3:  Exit  {}
end

event 56
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  Set  {"MapVar1", Value = 1}
  2:  Add  {"Inventory", Value = 275}         -- "Flying Fist"
  3:  Exit  {}
end

event 57
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  Set  {"MapVar2", Value = 1}
  2:  Add  {"Inventory", Value = 275}         -- "Flying Fist"
  3:  Exit  {}
end

event 58
  0:  Cmp  {"MapVar3", Value = 1,   jump = 3}
  1:  Set  {"MapVar3", Value = 1}
  2:  Add  {"Inventory", Value = 275}         -- "Flying Fist"
  3:  Exit  {}
end

event 60
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 472,   jump = 15}         -- Temple of Fist once
  2:  Set  {"QBits", Value = 472}         -- Temple of Fist once
  3:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 2, X = 0, Y = -1036, Z = 257}
  4:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = -20, Y = -816, Z = 257}
  5:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = -376, Y = 185, Z = 1}
  6:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = 384, Y = -92, Z = 1}
  7:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = 830, Y = -29, Z = 1}
  8:  SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 3, X = 1349, Y = 413, Z = 1}
  9:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = 1668, Y = 1478, Z = 1}
  10: SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 3, X = 1194, Y = 1513, Z = 1}
  11: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 1, X = 251, Y = 1205, Z = 244}
  12: SummonMonsters  {TypeIndexInMapStats = 2, Level = 2, Count = 1, X = -708, Y = -50, Z = 1}
  13: SummonMonsters  {TypeIndexInMapStats = 2, Level = 1, Count = 2, X = -743, Y = 588, Z = 1}
  14: SummonMonsters  {TypeIndexInMapStats = 2, Level = 3, Count = 1, X = 346, Y = 475, Z = 289}
  15: Exit  {}
end
