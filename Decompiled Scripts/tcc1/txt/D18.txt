str[0] = " "
str[1] = "Door"
str[2] = "Lever"
str[3] = "The Door won't budge."
str[4] = "Tile"
str[5] = "Chest"
str[6] = "The Door is locked"
str[7] = "Found something!"
str[8] = "Bookcase"
str[9] = "Exit"
str[10] = "Oozew Pit"
str[11] = ""


event 1
      Hint = str[1]  -- "Door"
      MazeInfo = str[10]  -- "Oozew Pit"
  0:  SetDoorState  {Id = 2, State = 1}
end

event 2
  0:  SetDoorState  {Id = 1, State = 1}
  1:  SetDoorState  {Id = 2, State = 1}
end

event 3
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 3, State = 1}
  1:  SetDoorState  {Id = 4, State = 1}
end

event 4
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 5, State = 1}
  1:  SetDoorState  {Id = 6, State = 1}
end

event 5
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 7, State = 1}
  1:  SetDoorState  {Id = 8, State = 1}
end

event 6
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 9, State = 1}
  1:  SetDoorState  {Id = 10, State = 1}
end

event 7
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 11, State = 1}
  1:  SetDoorState  {Id = 12, State = 1}
end

event 8
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 13, State = 1}
  1:  SetDoorState  {Id = 14, State = 1}
  2:  SetDoorState  {Id = 49, State = 0}
end

event 9
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 15, State = 1}
  1:  SetDoorState  {Id = 16, State = 1}
end

event 10
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 17, State = 1}
  1:  SetDoorState  {Id = 18, State = 1}
end

event 11
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 19, State = 1}
  1:  SetDoorState  {Id = 20, State = 1}
end

event 12
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 21, State = 1}
  1:  SetDoorState  {Id = 22, State = 1}
end

event 13
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 23, State = 1}
  1:  SetDoorState  {Id = 24, State = 1}
end

event 14
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 25, State = 1}
  1:  SetDoorState  {Id = 26, State = 1}
end

event 15
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 27, State = 2}         -- switch state
  1:  SetDoorState  {Id = 28, State = 2}         -- switch state
end

event 16
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 29, State = 1}
  1:  SetDoorState  {Id = 30, State = 1}
  2:  Exit  {}
end

event 17
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 31, State = 1}
  1:  SetDoorState  {Id = 32, State = 1}
end

event 18
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 33, State = 1}
  1:  SetDoorState  {Id = 34, State = 1}
end

event 19
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 35, State = 1}
  1:  SetDoorState  {Id = 36, State = 1}
end

event 20
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 37, State = 1}
  1:  SetDoorState  {Id = 38, State = 1}
end

event 21
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 39, State = 1}
  1:  SetDoorState  {Id = 40, State = 1}
end

event 22
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 22, State = 1}
end

event 23
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 23, State = 1}
end

event 24
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 24, State = 1}
end

event 25
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 25, State = 1}
end

event 26
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 26, State = 1}
end

event 29
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 29, State = 1}
end

event 30
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 30, State = 1}
end

event 31
      Hint = str[4]  -- "Tile"
  0:  Set  {"MapVar0", Value = 0}
  1:  MoveToMap  {X = 1916, Y = 6618, Z = 1, Direction = 502, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 32
      Hint = str[4]  -- "Tile"
  0:  Set  {"MapVar0", Value = 0}
  1:  MoveToMap  {X = -2688, Y = 1152, Z = 1152, Direction = 1550, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 33
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 48, State = 2}         -- switch state
  1:  SetDoorState  {Id = 41, State = 2}         -- switch state
end

event 34
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 42, State = 2}         -- switch state
  1:  SetDoorState  {Id = 50, State = 2}         -- switch state
end

event 35
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 43, State = 2}         -- switch state
  1:  SetDoorState  {Id = 53, State = 2}         -- switch state
end

event 36
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 44, State = 2}         -- switch state
  1:  SetDoorState  {Id = 51, State = 2}         -- switch state
end

event 38
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 45, State = 2}         -- switch state
  1:  SetDoorState  {Id = 48, State = 2}         -- switch state
end

event 39
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 46, State = 2}         -- switch state
  1:  SetDoorState  {Id = 52, State = 2}         -- switch state
  2:  SetDoorState  {Id = 49, State = 0}
end

event 40
      Hint = str[2]  -- "Lever"
  0:  SetDoorState  {Id = 47, State = 2}         -- switch state
  1:  SetDoorState  {Id = 49, State = 2}         -- switch state
end

event 41
      Hint = str[4]  -- "Tile"
  0:  Set  {"MapVar0", Value = 0}
  1:  MoveToMap  {X = -1822, Y = 4049, Z = 1, Direction = 502, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 42
      Hint = str[4]  -- "Tile"
  0:  Set  {"MapVar0", Value = 0}
  1:  MoveToMap  {X = 134, Y = 1151, Z = 1, Direction = 502, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 43
      Hint = str[4]  -- "Tile"
  0:  Set  {"MapVar0", Value = 0}
  1:  MoveToMap  {X = 2324, Y = -141, Z = -2047, Direction = 896, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 44
      Hint = str[5]  -- "Chest"
  0:  OpenChest  {Id = 1}
end

event 45
      Hint = str[5]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 46
      Hint = str[5]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 47
      Hint = str[5]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 48
      Hint = str[5]  -- "Chest"
  0:  Cmp  {"MapVar49", Value = 1,   jump = 3}
  1:  Cmp  {"QBits", Value = 5,   jump = 7}         --  5 D18, questbit for getting hourglass
  2:  Set  {"MapVar49", Value = 1}
  3:  OpenChest  {Id = 5}
  4:  Set  {"QBits", Value = 5}         --  5 D18, questbit for getting hourglass
  5:  Set  {"QBits", Value = 183}         -- Quest item bits for seer
  6:  Exit  {}

  7:  OpenChest  {Id = 8}
end

event 49
      Hint = str[5]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 50
      Hint = str[5]  -- "Chest"
  0:  OpenChest  {Id = 7}
end

event 51
  0:  Set  {"MapVar0", Value = 1}
end

event 52
  0:  OnLoadMap  {}
  1:  Set  {"MapVar0", Value = 0}
end

event 53
  0:  Set  {"MapVar0", Value = 0}
end

event 54
  0:  OnTimer  {IntervalInHalfMinutes = 2}
  1:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  2:  Exit  {}

  3:  DamagePlayer  {Player = "All", DamageType = const.Damage.Magic, Damage = 25}
end

event 55
      Hint = str[1]  -- "Door"
  0:  StatusText  {Str = 3}         -- "The Door won't budge."
end

event 56
  0:  OnTimer  {IntervalInHalfMinutes = 1}
  1:  RandomGoTo  {jumpA = 2, jumpB = 4, jumpC = 6, jumpD = 8, jumpE = 10, jumpF = 12}
  2:  SummonObject  {Type = 1000, X = -1792, Y = 1536, Z = -508, Speed = 1000, Count = 2, RandomAngle = false}         -- flare
  3:  Exit  {}

  4:  SummonObject  {Type = 1050, X = -1024, Y = 1408, Z = -508, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
  5:  Exit  {}

  6:  SummonObject  {Type = 1000, X = -1920, Y = 640, Z = -508, Speed = 1000, Count = 2, RandomAngle = false}         -- flare
  7:  Exit  {}

  8:  SummonObject  {Type = 1050, X = -1664, Y = 256, Z = -508, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
  9:  Exit  {}

  10: SummonObject  {Type = 1050, X = -1280, Y = 128, Z = -508, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
  11: Exit  {}

  12: SummonObject  {Type = 1050, X = -1280, Y = -256, Z = -508, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
end

event 57
  0:  OnTimer  {IntervalInHalfMinutes = 1}
  1:  RandomGoTo  {jumpA = 2, jumpB = 4, jumpC = 6, jumpD = 8, jumpE = 10, jumpF = 0}
  2:  SummonObject  {Type = 1000, X = 2688, Y = 6016, Z = -765, Speed = 1000, Count = 2, RandomAngle = true}         -- flare
  3:  Exit  {}

  4:  SummonObject  {Type = 1050, X = 3328, Y = 5888, Z = -765, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
  5:  Exit  {}

  6:  SummonObject  {Type = 1000, X = 1664, Y = 5888, Z = -765, Speed = 1000, Count = 2, RandomAngle = false}         -- flare
  7:  Exit  {}

  8:  SummonObject  {Type = 1050, X = 2650, Y = 6528, Z = -765, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  9:  Exit  {}

  10: SummonObject  {Type = 1050, X = 2650, Y = 5248, Z = -765, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
end

event 58
  0:  OnTimer  {IntervalInHalfMinutes = 1}
  1:  RandomGoTo  {jumpA = 2, jumpB = 4, jumpC = 6, jumpD = 8, jumpE = 10, jumpF = 12}
  2:  SummonObject  {Type = 1000, X = 4608, Y = 256, Z = -2044, Speed = 1000, Count = 2, RandomAngle = true}         -- flare
  3:  Exit  {}

  4:  SummonObject  {Type = 1050, X = 4736, Y = 0, Z = -2044, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  5:  Exit  {}

  6:  SummonObject  {Type = 1050, X = 4736, Y = -640, Z = -2044, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
  7:  Exit  {}

  8:  SummonObject  {Type = 1000, X = 4608, Y = -2816, Z = -2044, Speed = 1000, Count = 2, RandomAngle = false}         -- flare
  9:  Exit  {}

  10: SummonObject  {Type = 1050, X = 3456, Y = -3456, Z = -2044, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  11: Exit  {}

  12: SummonObject  {Type = 1050, X = 2816, Y = -3072, Z = -2044, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
end

event 59
  0:  OnTimer  {IntervalInHalfMinutes = 1}
  1:  RandomGoTo  {jumpA = 2, jumpB = 4, jumpC = 6, jumpD = 8, jumpE = 0, jumpF = 0}
  2:  SummonObject  {Type = 1050, X = -2432, Y = -2304, Z = -2044, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
  3:  Exit  {}

  4:  SummonObject  {Type = 1050, X = 1408, Y = -1280, Z = -2044, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  5:  Exit  {}

  6:  SummonObject  {Type = 1050, X = 512, Y = -1152, Z = -2044, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  7:  Exit  {}

  8:  SummonObject  {Type = 1000, X = -1024, Y = -1024, Z = -2044, Speed = 1000, Count = 2, RandomAngle = false}         -- flare
end

event 60
  0:  OnTimer  {IntervalInHalfMinutes = 1}
  1:  RandomGoTo  {jumpA = 2, jumpB = 4, jumpC = 6, jumpD = 8, jumpE = 0, jumpF = 0}
  2:  SummonObject  {Type = 1050, X = 2560, Y = 2304, Z = -1404, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  3:  Exit  {}

  4:  SummonObject  {Type = 1000, X = 2560, Y = 1664, Z = -1404, Speed = 1000, Count = 2, RandomAngle = true}         -- flare
  5:  Exit  {}

  6:  SummonObject  {Type = 1050, X = 3456, Y = 2304, Z = -1404, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  7:  Exit  {}

  8:  SummonObject  {Type = 1050, X = 3456, Y = 1792, Z = -1404, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
end

event 61
  0:  OnTimer  {IntervalInHalfMinutes = 1}
  1:  RandomGoTo  {jumpA = 2, jumpB = 4, jumpC = 6, jumpD = 8, jumpE = 10, jumpF = 0}
  2:  SummonObject  {Type = 1000, X = 896, Y = 2816, Z = -765, Speed = 1000, Count = 2, RandomAngle = true}         -- flare
  3:  Exit  {}

  4:  SummonObject  {Type = 1050, X = 1280, Y = 2432, Z = -765, Speed = 1000, Count = 2, RandomAngle = true}         -- fireball
  5:  Exit  {}

  6:  SummonObject  {Type = 1000, X = -512, Y = 2688, Z = -765, Speed = 1000, Count = 2, RandomAngle = false}         -- flare
  7:  Exit  {}

  8:  SummonObject  {Type = 1050, X = -1024, Y = 2304, Z = -765, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
  9:  Exit  {}

  10: SummonObject  {Type = 1050, X = -256, Y = 2816, Z = -765, Speed = 1000, Count = 2, RandomAngle = false}         -- fireball
end

event 62
      Hint = str[8]  -- "Bookcase"
  0:  Cmp  {"MapVar1", Value = 4,   jump = 15}
  1:  Add  {"MapVar1", Value = 1}
  2:  StatusText  {Str = 7}         -- "Found something!"
  3:  RandomGoTo  {jumpA = 4, jumpB = 6, jumpC = 8, jumpD = 10, jumpE = 12, jumpF = 14}
  4:  Add  {"Inventory", Value = 349}         -- "Guardian Angel"
  5:  Exit  {}

  6:  Add  {"Inventory", Value = 359}         -- "Cure Paralysis"
  7:  Exit  {}

  8:  Add  {"Inventory", Value = 362}         -- "Feeblemind"
  9:  Exit  {}

  10: Add  {"Inventory", Value = 372}         -- "Speed"
  11: Exit  {}

  12: Add  {"Inventory", Value = 353}         -- "Shared Life"
  13: Exit  {}

  14: Add  {"Inventory", Value = 350}         -- "Heroism"
  15: Exit  {}
end

event 90
      Hint = str[9]  -- "Exit"
  0:  MoveToMap  {X = -7537, Y = 4032, Z = 0, Direction = 0, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "OutD2.Odm"}
end

event 91
  0:  Cmp  {"MapVar44", Value = 1,   jump = 5}
  1:  Cmp  {"PlayerBits", Value = 30,   jump = 5}
  2:  Set  {"MapVar44", Value = 1}
  3:  Set  {"PlayerBits", Value = 30}
  4:  Add  {"FireResistance", Value = 25}
  5:  Exit  {}
end
