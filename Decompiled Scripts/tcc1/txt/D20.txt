str[0] = " "
str[1] = "Chest"
str[2] = "Exit"
str[3] = "Barrel"
str[4] = "Lair of Koschei"
str[5] = ""


event 1
      Hint = str[1]  -- "Chest"
      MazeInfo = str[4]  -- "Lair of Koschei"
  0:  OpenChest  {Id = 1}
end

event 2
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 2}
end

event 3
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 3}
end

event 4
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 4}
end

event 5
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 5}
end

event 6
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 6}
end

event 7
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 7}
end

event 9
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 8}
end

event 10
  0:  OpenChest  {Id = 9}
end

event 11
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 10}
end

event 12
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 11}
end

event 13
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 12}
end

event 14
      Hint = str[1]  -- "Chest"
  0:  OpenChest  {Id = 13}
end

event 15
      Hint = str[3]  -- "Barrel"
  0:  Cmp  {"MapVar0", Value = 1,   jump = 3}
  1:  Set  {"MapVar0", Value = 1}
  2:  GiveItem  {Strength = 6, Type = const.ItemType.Sword, Id = 5}         -- "Lionheart Sword"
  3:  Exit  {}
end

event 16
      Hint = str[3]  -- "Barrel"
  0:  Cmp  {"MapVar1", Value = 1,   jump = 3}
  1:  Set  {"MapVar1", Value = 1}
  2:  GiveItem  {Strength = 6, Type = const.ItemType.Armor_, Id = 78}         -- "Golden Plate Armor"
  3:  Exit  {}
end

event 17
      Hint = str[3]  -- "Barrel"
  0:  Cmp  {"MapVar2", Value = 1,   jump = 3}
  1:  Set  {"MapVar2", Value = 1}
  2:  GiveItem  {Strength = 6, Type = const.ItemType.Sword, Id = 8}         -- "Heroic Sword"
  3:  Exit  {}
end

event 18
      Hint = str[3]  -- "Barrel"
  0:  Cmp  {"MapVar3", Value = 1,   jump = 3}
  1:  Set  {"MapVar3", Value = 1}
  2:  GiveItem  {Strength = 6, Type = const.ItemType.Armor_, Id = 78}         -- "Golden Plate Armor"
  3:  Exit  {}
end

event 19
      Hint = str[3]  -- "Barrel"
  0:  Cmp  {"MapVar4", Value = 1,   jump = 3}
  1:  Set  {"MapVar4", Value = 1}
  2:  GiveItem  {Strength = 6, Type = const.ItemType.Axe, Id = 30}         -- "Grand Poleax"
  3:  Exit  {}
end

event 20
      Hint = str[3]  -- "Barrel"
  0:  Cmp  {"MapVar5", Value = 1,   jump = 3}
  1:  Set  {"MapVar5", Value = 1}
  2:  GiveItem  {Strength = 6, Type = const.ItemType.Spear, Id = 41}         -- "Titanic Trident"
  3:  Exit  {}
end

event 50
      Hint = str[2]  -- "Exit"
  0:  MoveToMap  {X = -13100, Y = 2028, Z = 161, Direction = 640, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "outb2.odm"}
end

event 100
  0:  OnLoadMap  {}
  1:  Cmp  {"QBits", Value = 507,   jump = 3}         -- Death Wish
  2:  Exit  {}

  3:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = -49, Y = 222, Z = -2}
  4:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = -49, Y = 789, Z = -2}
  5:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = 363, Y = 641, Z = -2}
  6:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = -733, Y = 1025, Z = -2}
  7:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = -284, Y = 3002, Z = -2}
  8:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = 744, Y = 4180, Z = -2}
  9:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = 139, Y = 6463, Z = -2}
  10: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = 414, Y = 8399, Z = 2}
  11: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 5, X = 1549, Y = 8982, Z = -2}
  12: ForPlayer  ("All")
  13: Set  {"Cursed", Value = 0}
end
