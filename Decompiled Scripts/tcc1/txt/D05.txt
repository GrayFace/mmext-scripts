str[0] = " "
str[1] = "Switch"
str[2] = "Door"
str[3] = "Lever"
str[4] = "Gold vein"
str[5] = "Gems"
str[6] = "Cave-in!"
str[7] = "Orpheus�s Gateway "
str[8] = "Black liquid filled barrel"
str[9] = "Old bones"
str[10] = "Glowing dinosaur bones"
str[11] = "Elevator platform"
str[12] = "Gateway Door"
str[13] = ""
str[14] = ""
str[15] = "Chest"
str[16] = ""
str[17] = ""
str[18] = "The door will not budge"
str[19] = ""
str[20] = ""
str[21] = ""
str[22] = ""
str[23] = ""
str[24] = "Bones"
str[25] = "The bones feel weird (+5 Poison resistance permanent)"
str[26] = "The bones feel weird (+5 Magic resistance permanent)"
str[27] = "No effect"
str[28] = "Only the Horn of Vainen can open the Gateway."
str[29] = "You blow the Horn of Vainen and the Gateway opens."
str[30] = "Exit"
str[31] = "The bones feel weird (+5 Might permanent)"
str[32] = ""


event 3
      Hint = str[1]  -- "Switch"
      MazeInfo = str[7]  -- "Orpheus�s Gateway "
  0:  SetDoorState  {Id = 3, State = 2}         -- switch state
  1:  SetDoorState  {Id = 2, State = 2}         -- switch state
end

event 4
      Hint = str[10]  -- "Glowing dinosaur bones"
  0:  MoveToMap  {X = 1576, Y = -1921, Z = 1, Direction = 44, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 0, Name = "0"}
end

event 6
      Hint = str[3]  -- "Lever"
  0:  SetDoorState  {Id = 6, State = 2}         -- switch state
  1:  SetDoorState  {Id = 5, State = 2}         -- switch state
end

event 7
  0:  SetDoorState  {Id = 7, State = 2}         -- switch state
end

event 8
      Hint = str[15]  -- "Chest"
  0:  OpenChest  {Id = 0}
end

event 9
      Hint = str[12]  -- "Gateway Door"
  0:  Cmp  {"QBits", Value = 397,   jump = 3}         -- Orpheus Gateway once
  1:  Cmp  {"Inventory", Value = 435,   jump = 4}         -- "Horn of Vainen"
  2:  StatusText  {Str = 28}         -- "Only the Horn of Vainen can open the Gateway."
  3:  Exit  {}

  4:  SetDoorState  {Id = 9, State = 1}
  5:  StatusText  {Str = 29}         -- "You blow the Horn of Vainen and the Gateway opens."
  6:  Subtract  {"Inventory", Value = 1}         -- "Longsword"
  7:  Add  {"QBits", Value = 397}         -- Orpheus Gateway once
  8:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = 387, Y = 8721, Z = 257}
  9:  SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = 420, Y = 8538, Z = 257}
  10: SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = 44, Y = 8517, Z = 247}
  11: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = -208, Y = 8507, Z = 257}
  12: SummonMonsters  {TypeIndexInMapStats = 1, Level = 2, Count = 3, X = -455, Y = 8588, Z = 247}
  13: SummonMonsters  {TypeIndexInMapStats = 1, Level = 3, Count = 3, X = -556, Y = 8573, Z = 257}
  14: Set  {"QBits", Value = 454}         -- Opened the Gateway
  15: Exit  {}

  140: StatusText  {Str = 28}         -- "Only the Horn of Vainen can open the Gateway."
end

event 10
      Hint = str[4]  -- "Gold vein"
  0:  Cmp  {"MapVar11", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 400}
  4:  SetTexture  {Facet = 204, Name = "T3LL6"}
  5:  Set  {"MapVar11", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 600}
  8:  SetTexture  {Facet = 204, Name = "T3LL6"}
  9:  Set  {"MapVar11", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 800}
  12: SetTexture  {Facet = 204, Name = "T3LL6"}
  13: Set  {"MapVar11", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 204, Name = "T3LL6"}
  18: Set  {"MapVar11", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 1000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 204, Name = "T3LL6"}
  24: Set  {"MapVar11", Value = 1}
  25: Exit  {}
end

event 11
      Hint = str[4]  -- "Gold vein"
  0:  Cmp  {"MapVar12", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 400}
  4:  SetTexture  {Facet = 207, Name = "T3LL6"}
  5:  Set  {"MapVar12", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 600}
  8:  SetTexture  {Facet = 207, Name = "T3LL6"}
  9:  Set  {"MapVar12", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 800}
  12: SetTexture  {Facet = 207, Name = "T3LL6"}
  13: Set  {"MapVar12", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 207, Name = "T3LL6"}
  18: Set  {"MapVar12", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 1000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 207, Name = "T3LL6"}
  24: Set  {"MapVar12", Value = 1}
  25: Exit  {}
end

event 12
      Hint = str[4]  -- "Gold vein"
  0:  Cmp  {"MapVar13", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 400}
  4:  SetTexture  {Facet = 12, Name = "T3LL6"}
  5:  Set  {"MapVar13", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 600}
  8:  SetTexture  {Facet = 12, Name = "T3LL6"}
  9:  Set  {"MapVar13", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 800}
  12: SetTexture  {Facet = 12, Name = "T3LL6"}
  13: Set  {"MapVar13", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 12, Name = "T3LL6"}
  18: Set  {"MapVar13", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 1000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 12, Name = "T3LL6"}
  24: Set  {"MapVar13", Value = 1}
  25: Exit  {}
end

event 13
      Hint = str[4]  -- "Gold vein"
  0:  Cmp  {"MapVar14", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 400}
  4:  SetTexture  {Facet = 624, Name = "T3LL6"}
  5:  Set  {"MapVar14", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 600}
  8:  SetTexture  {Facet = 624, Name = "T3LL6"}
  9:  Set  {"MapVar14", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 800}
  12: SetTexture  {Facet = 624, Name = "T3LL6"}
  13: Set  {"MapVar14", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 624, Name = "T3LL6"}
  18: Set  {"MapVar14", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 1000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 624, Name = "T3LL6"}
  24: Set  {"MapVar14", Value = 1}
  25: Exit  {}
end

event 14
      Hint = str[4]  -- "Gold vein"
  0:  Cmp  {"MapVar15", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 400}
  4:  SetTexture  {Facet = 612, Name = "T3LL6"}
  5:  Set  {"MapVar15", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 600}
  8:  SetTexture  {Facet = 612, Name = "T3LL6"}
  9:  Set  {"MapVar15", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 800}
  12: SetTexture  {Facet = 612, Name = "T3LL6"}
  13: Set  {"MapVar15", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 612, Name = "T3LL6"}
  18: Set  {"MapVar15", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 1000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 612, Name = "T3LL6"}
  24: Set  {"MapVar15", Value = 1}
  25: Exit  {}
end

event 21
      Hint = str[8]  -- "Black liquid filled barrel"
  0:  Add  {"PoisonedRed", Value = 3}
  1:  StatusText  {Str = 14}         -- ""
end

event 22
      Hint = str[8]  -- "Black liquid filled barrel"
  0:  Add  {"PoisonedRed", Value = 3}
  1:  StatusText  {Str = 14}         -- ""
end

event 23
      Hint = str[8]  -- "Black liquid filled barrel"
  0:  Add  {"PoisonedYellow", Value = 3}
  1:  StatusText  {Str = 14}         -- ""
end

event 25
      Hint = str[10]  -- "Glowing dinosaur bones"
  0:  Cmp  {"MapVar7", Value = 1,   jump = 7}
  1:  Cmp  {"BaseMight", Value = 40,   jump = 7}
  2:  Add  {"MapVar7", Value = 1}
  3:  Add  {"BaseMight", Value = 5}
  4:  StatusText  {Str = 31}         -- "The bones feel weird (+5 Might permanent)"
  5:  FaceExpression  {Player = "Current", Frame = 52}
  6:  Exit  {}

  7:  StatusText  {Str = 27}         -- "No effect"
end

event 26
      Hint = str[4]  -- "Gold vein"
  0:  Cmp  {"MapVar19", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 400}
  4:  SetTexture  {Facet = 143, Name = "T3LL6"}
  5:  Set  {"MapVar19", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 600}
  8:  SetTexture  {Facet = 143, Name = "T3LL6"}
  9:  Set  {"MapVar19", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 800}
  12: SetTexture  {Facet = 143, Name = "T3LL6"}
  13: Set  {"MapVar19", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 143, Name = "T3LL6"}
  18: Set  {"MapVar19", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 1000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 143, Name = "T3LL6"}
  24: Set  {"MapVar19", Value = 1}
  25: Exit  {}
end

event 28
      Hint = str[5]  -- "Gems"
  0:  Cmp  {"MapVar16", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 600}
  4:  SetTexture  {Facet = 124, Name = "T3LL6"}
  5:  Set  {"MapVar16", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 800}
  8:  SetTexture  {Facet = 124, Name = "T3LL6"}
  9:  Set  {"MapVar16", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 1200}
  12: SetTexture  {Facet = 124, Name = "T3LL6"}
  13: Set  {"MapVar16", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 124, Name = "T3LL6"}
  18: Set  {"MapVar16", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 2000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 124, Name = "T3LL6"}
  24: Set  {"MapVar16", Value = 1}
  25: Exit  {}
end

event 29
      Hint = str[5]  -- "Gems"
  0:  Cmp  {"MapVar17", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 600}
  4:  SetTexture  {Facet = 132, Name = "T3LL6"}
  5:  Set  {"MapVar17", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 800}
  8:  SetTexture  {Facet = 132, Name = "T3LL6"}
  9:  Set  {"MapVar17", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 1200}
  12: SetTexture  {Facet = 132, Name = "T3LL6"}
  13: Set  {"MapVar17", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 132, Name = "T3LL6"}
  18: Set  {"MapVar17", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 2000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 132, Name = "T3LL6"}
  24: Set  {"MapVar17", Value = 1}
  25: Exit  {}
end

event 30
      Hint = str[5]  -- "Gems"
  0:  Cmp  {"MapVar18", Value = 1,   jump = 2}
  1:  RandomGoTo  {jumpA = 15, jumpB = 20, jumpC = 15, jumpD = 11, jumpE = 7, jumpF = 3}
  2:  Exit  {}

  3:  Add  {"GoldAddRandom", Value = 600}
  4:  SetTexture  {Facet = 135, Name = "T3LL6"}
  5:  Set  {"MapVar18", Value = 1}
  6:  Exit  {}

  7:  Add  {"GoldAddRandom", Value = 800}
  8:  SetTexture  {Facet = 135, Name = "T3LL6"}
  9:  Set  {"MapVar18", Value = 1}
  10: Exit  {}

  11: Add  {"GoldAddRandom", Value = 1200}
  12: SetTexture  {Facet = 135, Name = "T3LL6"}
  13: Set  {"MapVar18", Value = 1}
  14: Exit  {}

  15: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  16: StatusText  {Str = 6}         -- "Cave-in!"
  17: SetTexture  {Facet = 135, Name = "T3LL6"}
  18: Set  {"MapVar18", Value = 1}
  19: Exit  {}

  20: DamagePlayer  {Player = "All", DamageType = const.Damage.Phys, Damage = 20}
  21: Add  {"Gold", Value = 2000}
  22: StatusText  {Str = 6}         -- "Cave-in!"
  23: SetTexture  {Facet = 135, Name = "T3LL6"}
  24: Set  {"MapVar18", Value = 1}
  25: Exit  {}
end

event 31
      Hint = str[18]  -- "The door will not budge"
  0:  StatusText  {Str = 18}         -- "The door will not budge"
end

event 32
      Hint = str[24]  -- "Bones"
  0:  Cmp  {"MapVar9", Value = 1,   jump = 5}
  1:  Add  {"MapVar9", Value = 1}
  2:  Add  {"PoisonResistance", Value = 5}
  3:  StatusText  {Str = 25}         -- "The bones feel weird (+5 Poison resistance permanent)"
  4:  Exit  {}

  5:  StatusText  {Str = 27}         -- "No effect"
  6:  Exit  {}
end

event 33
      Hint = str[24]  -- "Bones"
  0:  Cmp  {"MapVar10", Value = 1,   jump = 5}
  1:  Add  {"MapVar10", Value = 1}
  2:  Add  {"MagicResistance", Value = 5}
  3:  StatusText  {Str = 26}         -- "The bones feel weird (+5 Magic resistance permanent)"
  4:  Exit  {}

  5:  StatusText  {Str = 27}         -- "No effect"
  6:  Exit  {}
end

event 35
  0:  OnLoadMap  {}
  1:  Cmp  {"MapVar11", Value = 1,   jump = 3}
  2:  GoTo  {jump = 4}

  3:  SetTexture  {Facet = 204, Name = "t3ll6"}
  4:  Cmp  {"MapVar12", Value = 1,   jump = 6}
  5:  GoTo  {jump = 7}

  6:  SetTexture  {Facet = 207, Name = "t3ll6"}
  7:  Cmp  {"MapVar13", Value = 1,   jump = 8}
  8:  GoTo  {jump = 10}

  9:  SetTexture  {Facet = 12, Name = "t3ll6"}
  10: Cmp  {"MapVar14", Value = 1,   jump = 12}
  11: GoTo  {jump = 13}

  12: SetTexture  {Facet = 624, Name = "t3ll6"}
  13: Cmp  {"MapVar15", Value = 1,   jump = 15}
  14: GoTo  {jump = 16}

  15: SetTexture  {Facet = 612, Name = "t3ll6"}
  16: Cmp  {"MapVar19", Value = 1,   jump = 18}
  17: GoTo  {jump = 19}

  18: SetTexture  {Facet = 143, Name = "t3ll6"}
  19: Cmp  {"MapVar16", Value = 1,   jump = 21}
  20: GoTo  {jump = 22}

  21: SetTexture  {Facet = 124, Name = "t3ll6"}
  22: Cmp  {"MapVar17", Value = 1,   jump = 24}
  23: GoTo  {jump = 25}

  24: SetTexture  {Facet = 132, Name = "t3ll6"}
  25: Cmp  {"MapVar18", Value = 1,   jump = 27}
  26: Exit  {}

  27: SetTexture  {Facet = 135, Name = "t3ll6"}
end

event 50
      Hint = str[30]  -- "Exit"
  0:  MoveToMap  {X = -17962, Y = 20974, Z = 1, Direction = 1152, LookAngle = 0, SpeedZ = 0, HouseId = 0, Icon = 6, Name = "OutC3.Odm"}
end

event 51
  0:  Cmp  {"MapVar24", Value = 1,   jump = 8}
  1:  Set  {"MapVar24", Value = 1}
  2:  RandomGoTo  {jumpA = 3, jumpB = 5, jumpC = 7, jumpD = 0, jumpE = 0, jumpF = 0}
  3:  GiveItem  {Strength = 5, Type = const.ItemType.Ring_, Id = 0}
  4:  Exit  {}

  5:  GiveItem  {Strength = 5, Type = const.ItemType.Weapon_, Id = 0}
  6:  Exit  {}

  7:  GiveItem  {Strength = 5, Type = const.ItemType.Armor_, Id = 0}
  8:  Exit  {}
end
